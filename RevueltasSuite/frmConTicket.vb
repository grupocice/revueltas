﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 15/02/2017
' Hora: 09:39 a. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports Oracle.DataAccess.Client
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Imports System.Net
Imports System.Data.OleDb
'
Public Partial Class frmConTicket
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		Me.InitializeComponent()
		
		'
		' TODO : Add constructor code after InitializeComponents
		'
	End Sub
	'
	Public Sub LimpiaPantalla
		txtPesID.Text = vbNullString
		txtPlaca.Text = vbNullString
		txtChofer.Text = vbNullString
		txtEmpresa.Text = vbNullString
		txtProducto.Text = vbNullString
		txtBruto.Text = vbNullString
		txtTara.Text = vbNullString
		txtNeto.Text = vbNullString
	End Sub
	'
	Public Sub BuscaBoleta(pBoleta As String)
		'
		Dim vQuery As String
		' Generamos el query para buscar los datos del pesaje 
		vQuery = "SELECT count(Pes_ID) as Cantidad FROM Pesadas " & _
		         " where Pes_ID = " + pBoleta
		'
		GetDataJMT(vQuery)
		'
	End Sub
	
	Public Sub SendAdicionales(pBoleta As String)
		'
		Dim vQuery As String
		' Generamos el query para buscar los datos adicionales capturados en el pesaje 
		vQuery = "SELECT PES_ID, CAR_ID, PVA_NUMERO, VXC_ID, PVA_Valor FROM dba.PesValAdi " & _
		         " where Pes_ID = " + pBoleta
		'
		GetDataOLE(vQuery)
		Dim nCantidad As Integer = dsEnvios.Tables(0).Rows.Count
        If nCantidad > 0 Then
			'
			Dim vPes_Id As String =  dsEnvios.Tables(0).Rows(0)("Pes_Id").ToString
			Dim vCar_Id As String =  dsEnvios.Tables(0).Rows(0)("Car_Id").ToString
			Dim vPva_Numero As String =  dsEnvios.Tables(0).Rows(0)("Pva_Numero").ToString
			Dim vVxc_Id As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Vxc_Id").ToString + Chr(39)
			Dim vPva_Valor As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pva_Valor").ToString + Chr(39)
			'
			vQuery = "Insert Into PesValAdi(PES_ID, CAR_ID, PVA_NUMERO, VXC_ID, PVA_Valor) " & _
				    " Values (" + vPes_ID + "," + vCar_Id + "," + vPva_Numero + "," + vVxc_Id + "," + vPva_Valor + ")"
			'
			Try
			   	Dim ddataAdapter = New OracleDataAdapter(vQuery, xjmt)
			   	ddataAdapter.SelectCommand.ExecuteNonQuery()
		   	Catch ex As Exception
				MessageBox.Show("Error PesValAdi" & vbCrLf & ex.Message)
			End Try	
		End If
		   '
	End Sub
	'
	Public Sub SendData()
		Dim vQuery As String
		'
		Dim ddataRow As DataRow
		'
		Dim vPes_ID As String
		Dim vPes_PedID As String 
		Dim vOrd_ID As String
		Dim vPes_Placas As String
		Dim vPes_Chofer As String
		Dim vEmp_Nombre As String
		Dim vPro_Nombre As String
		Dim vPes_Bruto As String
		Dim vPes_Tara As String 
		Dim vPes_Neto As String
		Dim vPes_UnidadPri As String
		'
		Dim vFecha As String
		Dim dFecha As Date
		'
		ddataRow = dsEnvios.Tables(0).Rows(0)
		'
		' Formación de Cadenas para el envio de pruebas
		vPes_ID  =  Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_ID").ToString + Chr(39)
		'
		'
		vPes_PedID = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_PedID").ToString + Chr(39)
		If Trim(dsEnvios.Tables(0).Rows(0)("Ord_ID").ToString) Is "" Then
			vOrd_ID = Chr(39) + dsEnvios.Tables(0).Rows(0)("Ord_ID").ToString + Chr(39)
		Else
			vOrd_ID = dsEnvios.Tables(0).Rows(0)("Ord_ID").ToString
		End If
		'
		vPes_Placas = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_Placas").ToString + Chr(39)
		vPes_Chofer = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_Chofer").ToString + Chr(39)
		vEmp_Nombre = Chr(39) + dsEnvios.Tables(0).Rows(0)("Emp_Nombre").ToString + Chr(39)
		vPro_Nombre = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pro_Nombre").ToString + Chr(39)
		vPes_Bruto = dsEnvios.Tables(0).Rows(0)("Pes_Bruto").ToString
		vPes_Tara = dsEnvios.Tables(0).Rows(0)("Pes_Tara").ToString.ToString
		vPes_Neto = dsEnvios.Tables(0).Rows(0)("Pes_Neto").ToString
		vPes_UnidadPri = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_UnidadPri").ToString + Chr(39)
		Dim vPes_BasPri As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_BasPri").ToString + Chr(39)
		Dim vPes_OpeNomPri As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_OpeNomPri").ToString + Chr(39)
		Dim vPes_ObsPri As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_ObsPri").ToString + Chr(39)
		Dim vPes_PesoPri As String = dsEnvios.Tables(0).Rows(0)("Pes_PesoPri").ToString
		Dim vPes_Mod2Pri As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_Mod2Pri").ToString + Chr(39)
		Dim vPes_BasSeg As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_BasSeg").ToString + Chr(39)
		Dim vPes_OpeNomSeg As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_OpeNomSeg").ToString + Chr(39)
		Dim vPes_FecHorSeg As String 
		Dim vPes_ObsSeg As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_ObsSeg").ToString + Chr(39)
		Dim vPes_PesoSeg As String = dsEnvios.Tables(0).Rows(0)("Pes_PesoSeg").ToString
		Dim vPes_Mod2Seg As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_Mod2Seg").ToString + Chr(39)
		Dim vPes_FecHor As String 
		Dim vPes_FecHorPri As String 
		Dim vPes_NetoMerma As String
		Dim vUsb_ID As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Usb_ID").ToString + Chr(39)
		Dim vUsb_Nombre As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Usb_Nombre").ToString + Chr(39)
		Dim vPro_ID As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pro_ID").ToString + Chr(39)
		Dim vEmp_ID As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Emp_ID").ToString + Chr(39)
		Dim vEmp_Categoria As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Emp_Categoria").ToString + Chr(39)
		Dim vTra_ID As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Tra_ID").ToString + Chr(39)
		Dim vTra_Nombre As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Tra_Nombre").ToString + Chr(39)
		Dim vTag_ID As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Tag_ID").ToString + Chr(39)
		Dim vPes_CamVagon As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_CamVagon").ToString + Chr(39)
		'
		Dim vPes_TipoPesada As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_TipoPesada").ToString + Chr(39)
		Dim vPes_PriConv As String
		Dim vPes_SegConv As String
		Dim vPes_TerConv As String
		Dim vPes_BrutoConv As String
		Dim vPes_TaraConv As String
		Dim vPes_NetoConv As String
		Dim vPes_Adicional As String = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_Adicional").ToString + Chr(39)
		Dim vPes_Completo As String
		Dim vPes_CantImp As String
		'
		If Trim(vPes_PesoSeg) = vbNullString Then vPes_PesoSeg = "NULL" Else vPes_PesoSeg = dsEnvios.Tables(0).Rows(0)("Pes_PesoSeg").ToString 
		If Trim(vPes_PesoPri) = vbNullString Then vPes_PesoPri = "NULL" Else vPes_PesoSeg = dsEnvios.Tables(0).Rows(0)("Pes_PesoPri").ToString
		'
		vPes_Completo = dsEnvios.Tables(0).Rows(0)("Pes_Completo").ToString
		If vPes_Completo = "True" Then
			vPes_Completo = Chr(39) + "1" + Chr(39)
		Else
			vPes_Completo = Chr(39) + "0" + Chr(39)
		End If
		'
		vFecha = dsEnvios.Tables(0).Rows(0)("Pes_FecHor").ToString
		If Trim(vFecha) <> vbNullString Then
			dFecha = Convert.ToDateTime(dsEnvios.Tables(0).Rows(0)("Pes_FecHor").ToString)
		   	vFecha = dFecha.ToString("dd/MM/yyyy HH:mm:ss")
		   	vPes_FecHor = "TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + ")"
		Else
			vPes_FecHor = "NULL"
		End If
		'
		vFecha = dsEnvios.Tables(0).Rows(0)("Pes_FecHorPri").ToString
		If Trim(vFecha) <> vbNullString Then
			dFecha = Convert.ToDateTime(dsEnvios.Tables(0).Rows(0)("Pes_FecHorPri").ToString)
			vFecha = dFecha.ToString("dd/MM/yyyy HH:mm:ss")
			vPes_FecHorPri = "TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + ")"
		Else
			vPes_FecHorPri = "NULL"
		End If
		'
		vFecha = dsEnvios.Tables(0).Rows(0)("Pes_FecHorSeg").ToString
		If Trim(vFecha) <> vbNullString Then
			dFecha = Convert.ToDateTime(dsEnvios.Tables(0).Rows(0)("Pes_FecHorSeg").ToString)
			vFecha = dFecha.ToString("dd/MM/yyyy HH:mm:ss")
			vPes_FecHorSeg = "TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + ")"
		Else
			vPes_FecHorSeg = "NULL"
		End If
		'
		If Trim(dsEnvios.Tables(0).Rows(0)("Pes_NetoMerma").ToString) = "" Then vPes_NetoMerma = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_NetoMerma").ToString + Chr(39) Else vPes_NetoMerma = dsEnvios.Tables(0).Rows(0)("Pes_NetoMerma").ToString
		If Trim(dsEnvios.Tables(0).Rows(0)("Pes_CantImp").ToString) = "" Then vPes_CantImp = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_CantImp").ToString + Chr(39) Else vPes_CantImp = dsEnvios.Tables(0).Rows(0)("Pes_CantImp").ToString
		If Trim(dsEnvios.Tables(0).Rows(0)("Pes_PriConv").ToString) Is "" Then vPes_PriConv = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_PriConv").ToString + Chr(39) Else vPes_PriConv = dsEnvios.Tables(0).Rows(0)("Pes_PriConv").ToString
	    If Trim(dsEnvios.Tables(0).Rows(0)("Pes_SegConv").ToString) Is "" Then vPes_SegConv = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_SegConv").ToString + Chr(39) Else vPes_SegConv = dsEnvios.Tables(0).Rows(0)("Pes_SegConv").ToString 
	    If Trim(dsEnvios.Tables(0).Rows(0)("Pes_TerConv").ToString) Is "" Then vPes_TerConv = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_TerConv").ToString + Chr(39) Else vPes_TerConv = dsEnvios.Tables(0).Rows(0)("Pes_TerConv").ToString 
	    If Trim(dsEnvios.Tables(0).Rows(0)("Pes_BrutoConv").ToString) Is "" Then vPes_BrutoConv = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_BrutoConv").ToString + Chr(39) Else vPes_BrutoConv = dsEnvios.Tables(0).Rows(0)("Pes_BrutoConv").ToString 
	    If Trim(dsEnvios.Tables(0).Rows(0)("Pes_TaraConv").ToString) Is "" Then vPes_TaraConv = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_TaraConv").ToString + Chr(39) Else vPes_TaraConv = dsEnvios.Tables(0).Rows(0)("Pes_TaraConv").ToString 
	    If Trim(dsEnvios.Tables(0).Rows(0)("Pes_NetoConv").ToString) Is "" Then vPes_NetoConv = Chr(39) + dsEnvios.Tables(0).Rows(0)("Pes_NetoConv").ToString + Chr(39) Else vPes_NetoConv = dsEnvios.Tables(0).Rows(0)("Pes_NetoConv").ToString
	    '	
	    If trim(dsEnvios.Tables(0).Rows(0)("Pes_Bruto").ToString) Is "" Then vPes_Bruto = Chr(39) + "0" + Chr(39) Else vPes_Bruto = dsEnvios.Tables(0).Rows(0)("Pes_Bruto").ToString
	    If trim(dsEnvios.Tables(0).Rows(0)("Pes_Tara").ToString) Is "" Then vPes_Tara = Chr(39) + "0" + Chr(39) Else vPes_Tara = dsEnvios.Tables(0).Rows(0)("Pes_Tara").ToString
	    If trim(dsEnvios.Tables(0).Rows(0)("Pes_Neto").ToString) Is "" Then vPes_Neto = Chr(39) + "0" + Chr(39) Else vPes_Neto = dsEnvios.Tables(0).Rows(0)("Pes_Neto").ToString
	    	'
	    If vExiste = False Then
			vQuery = "Insert Into Pesadas(Pes_ID, Pes_PedID, Ord_ID, Pes_BasPri, Pes_OpeNomPri, Pes_FecHorPri, Pes_ObsPri, Pes_PesoPri, Pes_Mod2Pri, Pes_BasSeg, Pes_OpeNomSeg, Pes_FecHorSeg, Pes_ObsSeg, Pes_PesoSeg, Pes_Mod2Seg, Pes_FecHor, " & _
				" Pes_Bruto, Pes_Tara, Pes_Neto, Pes_NetoMerma, Usb_ID, Usb_Nombre, Pro_ID, Pro_Nombre, Emp_ID, Emp_Nombre, Emp_Categoria, Tra_ID, Tra_Nombre, Pes_Chofer, Pes_Placas, Tag_ID, Pes_UnidadPri, Pes_TipoPesada, Pes_PriConv, " & _
				" Pes_SegConv, Pes_TerConv, Pes_BrutoConv, Pes_TaraConv, Pes_NetoConv, Pes_Adicional, Pes_Completo, Pes_CantImp, Pes_CamVagon) " & _
				" Values (" + vPes_ID + "," + vPes_PedID + "," + vOrd_ID + "," + vPes_BasPri + "," + vPes_OpeNomPri + "," + vPes_FecHorPri + "," + vPes_ObsPri + "," + vPes_PesoPri + "," + vPes_Mod2Pri + "," + vPes_BasSeg + "," + vPes_OpeNomSeg + "," + vPes_FecHorSeg + "," + vPes_ObsSeg + "," + vPes_PesoSeg + "," + vPes_Mod2Seg + "," + vPes_FecHor + "," + _
				 vPes_Bruto + "," + vPes_Tara + "," + vPes_Neto + "," + vPes_NetoMerma + "," + vUsb_ID + "," + vUsb_Nombre + "," + vPro_ID + "," + vPro_Nombre + "," + vEmp_ID + "," + vEmp_Nombre + "," + vEmp_Categoria + "," + vTra_ID + "," + vTra_Nombre + "," + vPes_Chofer + "," + vPes_Placas + "," + vTag_ID + "," + vPes_UnidadPri + "," + vPes_TipoPesada + "," + vPes_PriConv + "," + _
				 vPes_SegConv + "," + vPes_TerConv + "," + vPes_BrutoConv + "," + vPes_TaraConv + "," + vPes_NetoConv + "," + vPes_Adicional + "," + vPes_Completo + "," + vPes_CantImp + "," + vPes_CamVagon + ")"
	    Else
	        vQuery = "UPDATE PESADAS " & _
                     "   SET Pes_ID=" + vPes_ID + ", Pes_PedID = " + vPes_PedID + ", Ord_ID =" + vOrd_ID + ", Pes_BasPri=" + vPes_BasPri + ", Pes_OpeNomPri=" + vPes_OpeNomPri + ", Pes_FecHorPri=" + vPes_FecHorPri + ", Pes_ObsPri=" + vPes_ObsPri + ", Pes_PesoPri=" + vPes_PesoPri + ", Pes_Mod2Pri=" + vPes_Mod2Pri + ", Pes_BasSeg=" + vPes_BasSeg + ", Pes_OpeNomSeg=" + vPes_OpeNomSeg + ", Pes_FecHorSeg=" + vPes_FecHorSeg + ", " & _ 
                     "       Pes_ObsSeg=" + vPes_ObsSeg + ", Pes_PesoSeg=" + vPes_PesoSeg + ", Pes_Mod2Seg=" + vPes_Mod2Seg + ", Pes_FecHor="+ vPes_FecHor + ", Pes_Bruto="+vPes_Bruto + ", Pes_Tara=" + vPes_Tara + ", Pes_Neto=" + vPes_Neto + ",  Pes_NetoMerma=" + vPes_NetoMerma + ", Usb_ID=" + vUsb_ID + ", Usb_Nombre=" + vUsb_Nombre + ", Pro_ID=" + vPro_ID + ", Pro_Nombre=" + vPro_Nombre + ", Emp_ID=" + vEmp_ID + ", Emp_Nombre=" + vEmp_Nombre + ", " & _
                     "       Emp_Categoria=" + vEmp_Categoria + ", Tra_ID=" + vTra_ID + ", Tra_Nombre=" + vTra_Nombre + ", Pes_Chofer=" + vPes_Chofer + ", Pes_Placas=" + vPes_Placas + ", Tag_ID=" + vTag_ID + ", Pes_UnidadPri=" + vPes_UnidadPri + ", Pes_TipoPesada=" + vPes_TipoPesada + ", Pes_PriConv=" + vPes_PriConv + ", Pes_SegConv=" + vPes_SegConv + ", " & _ 
	                 "       Pes_TerConv=" + vPes_TerConv + ", Pes_BrutoConv=" + vPes_BrutoConv + ", Pes_TaraConv=" + vPes_TaraConv + ", Pes_NetoConv=" + vPes_NetoConv + ", Pes_Adicional=" + vPes_Adicional + ", Pes_Completo=" + vPes_Completo + ", Pes_CantImp=" + vPes_CantImp + ",  Pes_CamVagon=" + vPes_CamVagon + " where pes_id = " + vPes_ID 
	    End If    
	    '
		'MessageBox.Show(vQuery)
		Try
		   xjmt.Open
		   Dim ddataAdapter = New OracleDataAdapter(vQuery, xjmt)
		   ddataAdapter.SelectCommand.ExecuteNonQuery()
		   '
		   SendAdicionales(vPes_ID)
		Catch ex As Exception
			MessageBox.Show("Error" & vbCrLf & ex.Message)
		Finally
			xjmt.Close
		End Try		
		
		'
	End Sub
	'
	Public Sub GetDataJMT(ByVal selectCommand As String)
		'
		Dim Cadena As String
		Dim custDS As DataSet = New DataSet
		Dim selectCMD As OracleCommand = New OracleCommand(selectCommand, xjmt)
		Dim custDA As OracleDataAdapter = New OracleDataAdapter
		'
		Cadena = xjmt.ConnectionString
		selectCMD.CommandTimeout = 30
        custDA.SelectCommand = selectCMD
        custDA.Fill(custDS, "Datos")
        '
        If custDS.Tables(0).Rows.Count = 0 Then
        	vExiste = False
        ElseIf custDS.Tables(0).Rows(0)("Cantidad").ToString = "0" Then
        	vExiste = False
        Else
        	vExiste = True
        End If
        'dsEnvios = custDS
        '
    End Sub
	'
	Public Sub GetDataOLE(ByVal selectCommand As String)
		'
		Dim Cadena As String
		Dim custDS As DataSet = New DataSet
		Dim selectCMD As OleDb.OleDbCommand = New OleDb.OleDbCommand(selectCommand, connOLE)
		Dim custDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
		'
		Cadena = connOLE.ConnectionString
		selectCMD.CommandTimeout = 30
        custDA.SelectCommand = selectCMD
        custDA.Fill(custDS, "Datos")
        '
        dsEnvios = custDS
        '
    End Sub
    '
    Public Sub ConectarOLE()
		Dim Cadena As String
		Cadena = System.Configuration.ConfigurationManager.ConnectionStrings("RevueltaSIP").ConnectionString
		Cadena = String.Format(Cadena, My.Settings.RevueltaServer, My.Settings.RevueltaUsuario, My.Settings.RevueltaPass)
		connOLE = New OleDb.OleDbConnection(Cadena)
    End Sub
    '
    Public Sub ConectarseJMT()
		Dim cadena As String
		Cadena = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
		
		Dim oradb As String 
		oradb = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
		oradb = String.Format(oradb, My.Settings.IP, My.Settings.Usuario, My.Settings.Contrasena)
		xjmt.ConnectionString = oradb
        '
	End Sub
	'
	
	Sub BtnConsultarClick(sender As Object, e As EventArgs)
		If txtBoleto.Text <> "" Then
			Dim vQuery As String
			Dim ddataRow As DataRow
			' Prueba con un dato x a Carga
			If (connOLE.State = System.Data.ConnectionState.Closed) Or (connOLE.State = System.Data.ConnectionState.Broken) Then
				ConectarOLE()
			End If
			If (connOLE.State = System.Data.ConnectionState.Closed) Then
				connOLE.Open
			End If
			'
			vQuery = "Select Pes_ID, Pes_PedID, Ord_ID, Pes_BasPri, Pes_OpeNomPri, Pes_FecHorPri, Pes_ObsPri, Pes_PesoPri, Pes_Mod2Pri, Pes_BasSeg, Pes_OpeNomSeg, Pes_FecHorSeg, Pes_ObsSeg, Pes_PesoSeg, " & _
			         " Pes_Mod2Seg, Pes_FecHor, Pes_Bruto, Pes_Tara, Pes_Neto, Pes_NetoMerma, Usb_ID, Usb_Nombre, Pro_ID, Pro_Nombre, Emp_ID, Emp_Nombre, Emp_Categoria, Tra_ID, Tra_Nombre, Pes_Chofer,  "  & _
			         " Pes_Placas, Tag_ID, Pes_UnidadPri, Pes_TipoPesada, Pes_PriConv, Pes_SegConv, Pes_TerConv, Pes_BrutoConv, Pes_TaraConv, Pes_NetoConv, Pes_Adicional, Pes_Completo, Pes_CantImp, Pes_CamVagon from dba.Pesadas "  & _
	                 " where Pes_ID = " + txtBoleto.Text
			GetDataOLE(vQuery)
			'
			ConectarseJMT()
			'
			BuscaBoleta(txtBoleto.Text)
			'
			Dim nCantidad As Integer = dsEnvios.Tables(0).Rows.Count
			
			If nCantidad > 0 Then
				ddataRow = dsEnvios.Tables(0).Rows(0)
				'
				If ddataRow!Pes_CamVagon.ToString = "C" Then
					txtPesID.Text = "TractoCamion"
					txtPlaca.Text = ddataRow!Pes_Placas.ToString
				Else
					txtPesID.Text = "Ferroviario"
					txtPlaca.Text = ddataRow!Tag_ID.ToString
				End If
				'
				txtChofer.Text = ddataRow!Pes_Chofer.ToString
				txtEmpresa.Text = ddataRow!Emp_Nombre.ToString
				txtProducto.Text = ddataRow!Pro_Nombre.ToString
				txtBruto.Text = ddataRow!Pes_Bruto.ToString
				txtTara.Text = ddataRow!Pes_Tara.ToString
				txtNeto.Text = ddataRow!Pes_Neto.ToString
			   '
			Else
				MessageBox.Show("No Existe el registro del Boleto de Pesaje")
				LimpiaPantalla
			End If
			'
			
		End If
		'
		txtBoleto.Focus
		txtBoleto.SelectAll
		'		
	End Sub
	
	Sub BtnEnvioClick(sender As Object, e As EventArgs)
		Dim nCantidad As Integer = dsEnvios.Tables(0).Rows.Count
		Dim nError As Integer
		If nCantidad < 1 Or Trim(txtBoleto.Text) = "" Then
			MessageBox.Show("No se ha capturado Boleta de Pesaje")
			txtBoleto.Focus
			txtBoleto.SelectAll
		Else
			nError = 0
		Try
			xjmt.Close
			ConectarseJMT()
		    SendData()
		Catch ex As Exception
			nError = 1
			MessageBox.Show("No fue posible Enviar la boleta de pesaje" & vbCrLf & ex.Message)
		Finally
			If nError = 0 Then
				MessageBox.Show("Boleta de Pesaje Enviada con Exito")
			Else
				MessageBox.Show("Verifique su Configuracion")
			End If
		End Try
		'
		End If
	End Sub
	
	Sub FrmConTicketLoad(sender As Object, e As EventArgs)
		txtBoleto.Focus
	End Sub
	
	Sub FrmConTicketShown(sender As Object, e As EventArgs)
		txtBoleto.Focus
	End Sub
	
	Sub BtnSalirClick(sender As Object, e As EventArgs)
		conn.Close
		conn.Dispose
		'
		connOLE.Close
		connOLE.Dispose
		Me.Close
	End Sub
	
	Sub TxtBoletoKeyUp(sender As Object, e As KeyEventArgs)
		
	End Sub
	
	Sub TxtBoletoKeyPress(sender As Object, e As KeyPressEventArgs)
		Dim tmp As System.Windows.Forms.KeyPressEventArgs = e
		'
		If tmp.KeyChar = ChrW(Keys.Enter) Then
			Call BtnConsultarClick(sender, e)
		End if
	End Sub
End Class
