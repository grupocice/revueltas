﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 14/02/2017
' Hora: 03:06 p. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Public Partial Class MainForm
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		Me.InitializeComponent()
		
		'
		' TODO : Add constructor code after InitializeComponents
		'
	End Sub
	
	Sub SalirToolStripMenuItemClick(sender As Object, e As EventArgs)
		Me.Close
	End Sub
	
	Sub CalendarioToolStripMenuItemClick(sender As Object, e As EventArgs)
		mntCalendario.Visible = Not mntCalendario.Visible
	End Sub
	
	Sub ConsultarTicketsToolStripMenuItemClick(sender As Object, e As EventArgs)
		'
		MessageBox.Show("Consulta Ticket")
		'
	End Sub
	
	Sub EnviarTicketToolStripMenuItemClick(sender As Object, e As EventArgs)
		MessageBox.Show("Envia Ticket")
	End Sub
	
	Sub ModificarTicketToolStripMenuItemClick(sender As Object, e As EventArgs)
		Dim frmMod As New frmModificacion
		frmMod.MDIParent = Me
		frmMod.Show
	End Sub
	
	Sub SalirToolStripMenuItemMouseHover(sender As Object, e As EventArgs)
		toolStripStatusLabel1.Text = "Suite para Mantenimiento de Datos de Revueltas... " + salirToolStripMenuItem.ToolTipText
	End Sub
	
	Sub SalirToolStripMenuItemMouseLeave(sender As Object, e As EventArgs)
		toolStripStatusLabel1.Text = "Suite para Mantenimiento de Datos de Revueltas... " + vbNullString
	End Sub
	
	Sub CalendarioToolStripMenuItemMouseHover(sender As Object, e As EventArgs)
		toolStripStatusLabel1.Text = "Suite para Mantenimiento de Datos de Revueltas... " + calendarioToolStripMenuItem.ToolTipText
	End Sub
	
	Sub ListaDeTicketsToolStripMenuItemClick(sender As Object, e As EventArgs)
		Dim frmLista As New frmBoletos
		frmLista.MdiParent = Me
		frmLista.Show
	End Sub
	
	Sub ListaDeTicketsToolStripMenuItem1Click(sender As Object, e As EventArgs)
		Dim frmExiste As Form
		frmExiste = CheckForm(frmBoletos)
		If frmExiste Is Nothing Then
			Dim frmLista As New frmBoletos
			frmLista.MDIParent = Me
			frmLista.Show
		Else
			frmExiste.Activate
		End If
	End Sub
	
	Sub ModificarTicketsToolStripMenuItemClick(sender As Object, e As EventArgs)
		Dim frmExiste As Form
		frmExiste = CheckForm(frmModificacion)
		If frmExiste Is Nothing Then
			Dim frmMod As New frmModificacion
			frmMod.MDIParent = Me
			frmMod.Show
		Else
			frmExiste.Activate
		End If
	End Sub
	
	Sub UnTicketToolStripMenuItemClick(sender As Object, e As EventArgs)
		Dim frmExiste As Form
		frmExiste = CheckForm(frmConTicket)
		If frmExiste Is Nothing Then
			Dim frmConsulta As New frmConTicket
			frmConsulta.MdiParent = Me
			frmConsulta.Show
		Else
			frmExiste.Activate
		End If
	End Sub
	
	Sub CascadaToolStripMenuItemClick(sender As Object, e As EventArgs)
		Me.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade)
	End Sub
	
	Sub TituloToolStripMenuItemClick(sender As Object, e As EventArgs)
		Me.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical)
	End Sub
	
	Sub TituloHorizontalToolStripMenuItemClick(sender As Object, e As EventArgs)
		Me.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal)
	End Sub
	
	Private Function CheckForm(_form As Form) As Form
		'
		For Each f As Form In Application.OpenForms
			If f.Name = _form.Name Then
				Return f
			End If
		Next
		'
		Return Nothing
	
	End Function
	
	
	Sub MainFormLoad(sender As Object, e As EventArgs)
		Dim ctl As Control
		Dim ctlMDI As MdiClient
		'
		' Loop through all of the form's controls looking
		' for the control of type MdiClient.
		For Each ctl In Me.Controls
		    Try
		        ' Attempt to cast the control to type MdiClient.
		        ctlMDI = CType(ctl, MdiClient)
		
		        ' Set the BackColor of the MdiClient control.
		        ctlMDI.BackColor = Me.BackColor
		
		    Catch exc As InvalidCastException
		        ' Catch and ignore the error if casting failed.
		    End Try
		Next
		'
		ActualizarCatalogosToolStripMenuItemClick(sender, e)
	End Sub
	
	Sub ToolStripMenuItem6Click(sender As Object, e As EventArgs)
		' Configuracion
		Dim frmExiste As Form
		frmExiste = CheckForm(frmConfiguracion)
		If frmExiste Is Nothing Then
			Dim frmConfig As New frmConfiguracion
			frmConfig.MdiParent = Me
			frmConfig.Show
		Else
			frmExiste.Activate
		End If
	End Sub
	
	
	Sub AcercadeToolStripMenuItemClick(sender As Object, e As EventArgs)
		Dim frmExiste As Form
		frmExiste = CheckForm(FrmAcerca)
		If frmExiste Is Nothing Then
			Dim frmAbout As New FrmAcerca
			frmAbout.MdiParent = Me
			frmAbout.Show
		Else
			frmExiste.Activate
		End If
	End Sub
	
	Sub MnPrincipalMouseHover(sender As Object, e As EventArgs)
		'
		'MnPrincipal.
	End Sub
	
	Sub ActualizarCatalogosToolStripMenuItemClick(sender As Object, e As EventArgs)
		Dim frmExiste As Form
		frmExiste = CheckForm(frmUpdCatalogos)
		If frmExiste Is Nothing Then
			Dim frmCat As New frmUpdCatalogos
			'vCatalogos = False
			frmCat.MdiParent = Me
			frmCat.Show
		Else
			frmExiste.Activate
			'vCatalogos = True
		End If
	End Sub
	
	Sub MainFormResizeEnd(sender As Object, e As EventArgs)
		'
		Dim frmExiste As Form
        frmExiste = CheckForm(frmUpdCatalogos)
        frmExiste.Location = New Point(Me.Size.Width - (frmUpdCatalogos.Width + 30), Me.Size.Height - (frmUpdCatalogos.Height + 90))
        '
	End Sub
	
	
	Sub ToolStripMenuItem6MouseHover(sender As Object, e As EventArgs)
		'
		toolStripStatusLabel1.text = "Suite para Mantenimiento de Datos de Revueltas... " + sender.ToolTipText
		'
	End Sub
End Class
