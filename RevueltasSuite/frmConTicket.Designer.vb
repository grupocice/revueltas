﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 15/02/2017
' Hora: 09:39 a. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports Oracle.DataAccess.Client
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Net
Imports System.Data.OleDb
'
Partial Class frmConTicket
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConTicket))
		Me.label1 = New System.Windows.Forms.Label()
		Me.btnEnvio = New System.Windows.Forms.Button()
		Me.panel1 = New System.Windows.Forms.Panel()
		Me.txtNeto = New System.Windows.Forms.TextBox()
		Me.label9 = New System.Windows.Forms.Label()
		Me.label8 = New System.Windows.Forms.Label()
		Me.label7 = New System.Windows.Forms.Label()
		Me.label6 = New System.Windows.Forms.Label()
		Me.label5 = New System.Windows.Forms.Label()
		Me.label4 = New System.Windows.Forms.Label()
		Me.label10 = New System.Windows.Forms.Label()
		Me.txtPesID = New System.Windows.Forms.TextBox()
		Me.txtTara = New System.Windows.Forms.TextBox()
		Me.label3 = New System.Windows.Forms.Label()
		Me.txtBruto = New System.Windows.Forms.TextBox()
		Me.txtProducto = New System.Windows.Forms.TextBox()
		Me.txtEmpresa = New System.Windows.Forms.TextBox()
		Me.txtPlaca = New System.Windows.Forms.TextBox()
		Me.txtChofer = New System.Windows.Forms.TextBox()
		Me.btnConsultar = New System.Windows.Forms.Button()
		Me.label2 = New System.Windows.Forms.Label()
		Me.btnSalir = New System.Windows.Forms.Button()
		Me.txtBoleto = New System.Windows.Forms.TextBox()
		Me.dsEnvios = New System.Data.DataSet()
		Me.panel1.SuspendLayout
		CType(Me.dsEnvios,System.ComponentModel.ISupportInitialize).BeginInit
		Me.SuspendLayout
		'
		'label1
		'
		Me.label1.Location = New System.Drawing.Point(9, 283)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(118, 18)
		Me.label1.TabIndex = 48
		Me.label1.Text = "&Enviar"
		Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'btnEnvio
		'
		Me.btnEnvio.BackgroundImage = CType(resources.GetObject("btnEnvio.BackgroundImage"),System.Drawing.Image)
		Me.btnEnvio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
		Me.btnEnvio.Location = New System.Drawing.Point(9, 243)
		Me.btnEnvio.Name = "btnEnvio"
		Me.btnEnvio.Size = New System.Drawing.Size(119, 37)
		Me.btnEnvio.TabIndex = 44
		Me.btnEnvio.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.btnEnvio.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
		Me.btnEnvio.UseVisualStyleBackColor = true
		AddHandler Me.btnEnvio.Click, AddressOf Me.BtnEnvioClick
		'
		'panel1
		'
		Me.panel1.BackColor = System.Drawing.Color.White
		Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.panel1.Controls.Add(Me.txtNeto)
		Me.panel1.Controls.Add(Me.label9)
		Me.panel1.Controls.Add(Me.label8)
		Me.panel1.Controls.Add(Me.label7)
		Me.panel1.Controls.Add(Me.label6)
		Me.panel1.Controls.Add(Me.label5)
		Me.panel1.Controls.Add(Me.label4)
		Me.panel1.Controls.Add(Me.label10)
		Me.panel1.Controls.Add(Me.txtPesID)
		Me.panel1.Controls.Add(Me.txtTara)
		Me.panel1.Controls.Add(Me.label3)
		Me.panel1.Controls.Add(Me.txtBruto)
		Me.panel1.Controls.Add(Me.txtProducto)
		Me.panel1.Controls.Add(Me.txtEmpresa)
		Me.panel1.Controls.Add(Me.txtPlaca)
		Me.panel1.Controls.Add(Me.txtChofer)
		Me.panel1.Location = New System.Drawing.Point(9, 66)
		Me.panel1.Name = "panel1"
		Me.panel1.Size = New System.Drawing.Size(470, 168)
		Me.panel1.TabIndex = 47
		'
		'txtNeto
		'
		Me.txtNeto.Location = New System.Drawing.Point(311, 128)
		Me.txtNeto.Name = "txtNeto"
		Me.txtNeto.ReadOnly = true
		Me.txtNeto.Size = New System.Drawing.Size(100, 20)
		Me.txtNeto.TabIndex = 8
		'
		'label9
		'
		Me.label9.Location = New System.Drawing.Point(311, 108)
		Me.label9.Name = "label9"
		Me.label9.Size = New System.Drawing.Size(57, 14)
		Me.label9.TabIndex = 34
		Me.label9.Text = "Peso Neto"
		'
		'label8
		'
		Me.label8.Location = New System.Drawing.Point(166, 111)
		Me.label8.Name = "label8"
		Me.label8.Size = New System.Drawing.Size(65, 14)
		Me.label8.TabIndex = 33
		Me.label8.Text = "Tara"
		'
		'label7
		'
		Me.label7.Location = New System.Drawing.Point(19, 111)
		Me.label7.Name = "label7"
		Me.label7.Size = New System.Drawing.Size(63, 14)
		Me.label7.TabIndex = 32
		Me.label7.Text = "Peso Bruto"
		'
		'label6
		'
		Me.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.label6.Location = New System.Drawing.Point(19, 59)
		Me.label6.Name = "label6"
		Me.label6.Size = New System.Drawing.Size(50, 15)
		Me.label6.TabIndex = 31
		Me.label6.Text = "Producto"
		'
		'label5
		'
		Me.label5.Location = New System.Drawing.Point(231, 59)
		Me.label5.Name = "label5"
		Me.label5.Size = New System.Drawing.Size(50, 15)
		Me.label5.TabIndex = 30
		Me.label5.Text = "Empresa"
		'
		'label4
		'
		Me.label4.Location = New System.Drawing.Point(311, 6)
		Me.label4.Name = "label4"
		Me.label4.Size = New System.Drawing.Size(44, 17)
		Me.label4.TabIndex = 29
		Me.label4.Text = "Chofer"
		'
		'label10
		'
		Me.label10.Location = New System.Drawing.Point(19, 6)
		Me.label10.Name = "label10"
		Me.label10.Size = New System.Drawing.Size(51, 20)
		Me.label10.TabIndex = 39
		Me.label10.Text = "Tipo Transporte"
		'
		'txtPesID
		'
		Me.txtPesID.Location = New System.Drawing.Point(19, 26)
		Me.txtPesID.Name = "txtPesID"
		Me.txtPesID.ReadOnly = true
		Me.txtPesID.Size = New System.Drawing.Size(99, 20)
		Me.txtPesID.TabIndex = 1
		'
		'txtTara
		'
		Me.txtTara.Location = New System.Drawing.Point(165, 128)
		Me.txtTara.Name = "txtTara"
		Me.txtTara.ReadOnly = true
		Me.txtTara.Size = New System.Drawing.Size(100, 20)
		Me.txtTara.TabIndex = 7
		'
		'label3
		'
		Me.label3.Location = New System.Drawing.Point(166, 6)
		Me.label3.Name = "label3"
		Me.label3.Size = New System.Drawing.Size(50, 20)
		Me.label3.TabIndex = 28
		Me.label3.Text = "Placas"
		'
		'txtBruto
		'
		Me.txtBruto.Location = New System.Drawing.Point(19, 128)
		Me.txtBruto.Name = "txtBruto"
		Me.txtBruto.ReadOnly = true
		Me.txtBruto.Size = New System.Drawing.Size(100, 20)
		Me.txtBruto.TabIndex = 6
		'
		'txtProducto
		'
		Me.txtProducto.Location = New System.Drawing.Point(19, 77)
		Me.txtProducto.Name = "txtProducto"
		Me.txtProducto.ReadOnly = true
		Me.txtProducto.Size = New System.Drawing.Size(190, 20)
		Me.txtProducto.TabIndex = 4
		'
		'txtEmpresa
		'
		Me.txtEmpresa.Location = New System.Drawing.Point(215, 77)
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.txtEmpresa.ReadOnly = true
		Me.txtEmpresa.Size = New System.Drawing.Size(221, 20)
		Me.txtEmpresa.TabIndex = 5
		'
		'txtPlaca
		'
		Me.txtPlaca.Location = New System.Drawing.Point(166, 26)
		Me.txtPlaca.Name = "txtPlaca"
		Me.txtPlaca.ReadOnly = true
		Me.txtPlaca.Size = New System.Drawing.Size(99, 20)
		Me.txtPlaca.TabIndex = 2
		'
		'txtChofer
		'
		Me.txtChofer.Location = New System.Drawing.Point(311, 26)
		Me.txtChofer.Name = "txtChofer"
		Me.txtChofer.ReadOnly = true
		Me.txtChofer.Size = New System.Drawing.Size(125, 20)
		Me.txtChofer.TabIndex = 3
		'
		'btnConsultar
		'
		Me.btnConsultar.BackColor = System.Drawing.Color.Transparent
		Me.btnConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
		Me.btnConsultar.Image = CType(resources.GetObject("btnConsultar.Image"),System.Drawing.Image)
		Me.btnConsultar.Location = New System.Drawing.Point(130, 12)
		Me.btnConsultar.Name = "btnConsultar"
		Me.btnConsultar.Size = New System.Drawing.Size(110, 39)
		Me.btnConsultar.TabIndex = 43
		Me.btnConsultar.TabStop = false
		Me.btnConsultar.Text = "Consultar"
		Me.btnConsultar.UseVisualStyleBackColor = false
		AddHandler Me.btnConsultar.Click, AddressOf Me.BtnConsultarClick
		'
		'label2
		'
		Me.label2.Location = New System.Drawing.Point(9, 12)
		Me.label2.Name = "label2"
		Me.label2.Size = New System.Drawing.Size(93, 16)
		Me.label2.TabIndex = 46
		Me.label2.Text = "No. Boleto"
		'
		'btnSalir
		'
		Me.btnSalir.Location = New System.Drawing.Point(360, 243)
		Me.btnSalir.Name = "btnSalir"
		Me.btnSalir.Size = New System.Drawing.Size(119, 37)
		Me.btnSalir.TabIndex = 45
		Me.btnSalir.Text = "Cerrar"
		Me.btnSalir.UseVisualStyleBackColor = true
		AddHandler Me.btnSalir.Click, AddressOf Me.BtnSalirClick
		'
		'txtBoleto
		'
		Me.txtBoleto.Location = New System.Drawing.Point(9, 31)
		Me.txtBoleto.Name = "txtBoleto"
		Me.txtBoleto.Size = New System.Drawing.Size(93, 20)
		Me.txtBoleto.TabIndex = 42
		AddHandler Me.txtBoleto.KeyPress, AddressOf Me.TxtBoletoKeyPress
		AddHandler Me.txtBoleto.KeyUp, AddressOf Me.TxtBoletoKeyUp
		'
		'dsEnvios
		'
		Me.dsEnvios.DataSetName = "NewDataSet"
		'
		'frmConTicket
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.ClientSize = New System.Drawing.Size(482, 302)
		Me.ControlBox = false
		Me.Controls.Add(Me.label1)
		Me.Controls.Add(Me.btnEnvio)
		Me.Controls.Add(Me.panel1)
		Me.Controls.Add(Me.btnConsultar)
		Me.Controls.Add(Me.label2)
		Me.Controls.Add(Me.btnSalir)
		Me.Controls.Add(Me.txtBoleto)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.KeyPreview = true
		Me.MaximizeBox = false
		Me.MinimizeBox = false
		Me.Name = "frmConTicket"
		Me.ShowIcon = false
		Me.Text = "Consulta de Ticket"
		AddHandler Load, AddressOf Me.FrmConTicketLoad
		AddHandler Shown, AddressOf Me.FrmConTicketShown
		Me.panel1.ResumeLayout(false)
		Me.panel1.PerformLayout
		CType(Me.dsEnvios,System.ComponentModel.ISupportInitialize).EndInit
		Me.ResumeLayout(false)
		Me.PerformLayout
	End Sub
	Private dsEnvios As System.Data.DataSet
	Public txtBoleto As System.Windows.Forms.TextBox
	Private btnSalir As System.Windows.Forms.Button
	Private label2 As System.Windows.Forms.Label
	Private btnConsultar As System.Windows.Forms.Button
	Private txtChofer As System.Windows.Forms.TextBox
	Private txtPlaca As System.Windows.Forms.TextBox
	Private txtEmpresa As System.Windows.Forms.TextBox
	Private txtProducto As System.Windows.Forms.TextBox
	Private txtBruto As System.Windows.Forms.TextBox
	Private label3 As System.Windows.Forms.Label
	Private txtTara As System.Windows.Forms.TextBox
	Private txtPesID As System.Windows.Forms.TextBox
	Private label10 As System.Windows.Forms.Label
	Private label4 As System.Windows.Forms.Label
	Private label5 As System.Windows.Forms.Label
	Private label6 As System.Windows.Forms.Label
	Private label7 As System.Windows.Forms.Label
	Private label8 As System.Windows.Forms.Label
	Private label9 As System.Windows.Forms.Label
	Private txtNeto As System.Windows.Forms.TextBox
	Private panel1 As System.Windows.Forms.Panel
	Public btnEnvio As System.Windows.Forms.Button
	Private label1 As System.Windows.Forms.Label
	'
	Private conn    As New OracleConnection()
	'
	Private connSS  As New SqlClient.SqlConnection
	Private connOLE As New OleDb.OleDbConnection
	'
	Private connGrd As New OleDb.OleDbConnection 'SqlClient.SqlConnection
	Private xjmt    As New OracleConnection()
	Private vExiste As Boolean
End Class
