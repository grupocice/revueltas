﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 15/02/2017
' Hora: 03:51 p. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Partial Class FrmAcerca
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Me.label1 = New System.Windows.Forms.Label()
		Me.btnOk = New System.Windows.Forms.Button()
		Me.label2 = New System.Windows.Forms.Label()
		Me.SuspendLayout
		'
		'label1
		'
		Me.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.label1.Location = New System.Drawing.Point(12, 9)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(194, 105)
		Me.label1.TabIndex = 0
		Me.label1.Text = "Sistema para apoyo del Sistema de Revueltas"&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"Contiene los modulos requeridos pa"& _ 
		"ra consultar tickets de este sistema y permitiendo que pueda hacerse el envio a "& _ 
		"APEX"
		Me.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'btnOk
		'
		Me.btnOk.Location = New System.Drawing.Point(54, 146)
		Me.btnOk.Name = "btnOk"
		Me.btnOk.Size = New System.Drawing.Size(108, 35)
		Me.btnOk.TabIndex = 1
		Me.btnOk.Text = "Ok"
		Me.btnOk.UseVisualStyleBackColor = true
		AddHandler Me.btnOk.Click, AddressOf Me.BtnOkClick
		'
		'label2
		'
		Me.label2.Cursor = System.Windows.Forms.Cursors.Hand
		Me.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
		Me.label2.Location = New System.Drawing.Point(10, 118)
		Me.label2.Name = "label2"
		Me.label2.Size = New System.Drawing.Size(195, 14)
		Me.label2.TabIndex = 2
		Me.label2.Text = "emartinez@grupocice.com"
		Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		AddHandler Me.label2.Click, AddressOf Me.Label2Click
		'
		'FrmAcerca
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.ClientSize = New System.Drawing.Size(217, 191)
		Me.ControlBox = false
		Me.Controls.Add(Me.label2)
		Me.Controls.Add(Me.btnOk)
		Me.Controls.Add(Me.label1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MaximizeBox = false
		Me.MinimizeBox = false
		Me.Name = "FrmAcerca"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Acerca de"
		Me.ResumeLayout(false)
	End Sub
	Private label2 As System.Windows.Forms.Label
	Private btnOk As System.Windows.Forms.Button
	Private label1 As System.Windows.Forms.Label
End Class
