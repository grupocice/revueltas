﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 16/02/2017
' Hora: 10:13 a. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports System.Data.OleDb
Imports Oracle.DataAccess.Client
Imports System.Data
Imports System.Configuration
'
Partial Class frmUpdCatalogos
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdCatalogos))
		Me.Rojo = New System.Windows.Forms.PictureBox()
		Me.Verde = New System.Windows.Forms.PictureBox()
		Me.tabControl1 = New System.Windows.Forms.TabControl()
		Me.tabPage1 = New System.Windows.Forms.TabPage()
		Me.chklstNombre = New System.Windows.Forms.CheckedListBox()
		Me.chkList = New System.Windows.Forms.CheckedListBox()
		Me.lstUpdates = New System.Windows.Forms.ListBox()
		Me.btnPass = New System.Windows.Forms.Button()
		Me.tabPage2 = New System.Windows.Forms.TabPage()
		Me.lblFalta = New System.Windows.Forms.Label()
		Me.lblActivo = New System.Windows.Forms.Label()
		Me.txtPassword = New System.Windows.Forms.TextBox()
		Me.txtUsuario = New System.Windows.Forms.TextBox()
		Me.txtServidor = New System.Windows.Forms.TextBox()
		Me.label4 = New System.Windows.Forms.Label()
		Me.label3 = New System.Windows.Forms.Label()
		Me.label2 = New System.Windows.Forms.Label()
		Me.label1 = New System.Windows.Forms.Label()
		Me.UpDn = New System.Windows.Forms.NumericUpDown()
		Me.btnActivo = New System.Windows.Forms.Button()
		Me.rtTexto = New System.Windows.Forms.RichTextBox()
		Me.btnCerrar = New System.Windows.Forms.Button()
		Me.lstTablas = New System.Windows.Forms.ListBox()
		Me.lstKey = New System.Windows.Forms.ListBox()
		Me.dataGridView2 = New System.Windows.Forms.DataGridView()
		Me.dataGridView1 = New System.Windows.Forms.DataGridView()
		Me.mnuContextual = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.restaurarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
		Me.salirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.tmTiempo = New System.Windows.Forms.Timer(Me.components)
		Me.statusStrip1 = New System.Windows.Forms.StatusStrip()
		Me.toolText = New System.Windows.Forms.ToolStripStatusLabel()
		Me.toolStatus = New System.Windows.Forms.ToolStripStatusLabel()
		Me.tmpFalta = New System.Windows.Forms.Timer(Me.components)
		CType(Me.Rojo,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.Verde,System.ComponentModel.ISupportInitialize).BeginInit
		Me.tabControl1.SuspendLayout
		Me.tabPage1.SuspendLayout
		Me.tabPage2.SuspendLayout
		CType(Me.UpDn,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.dataGridView2,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.dataGridView1,System.ComponentModel.ISupportInitialize).BeginInit
		Me.mnuContextual.SuspendLayout
		Me.statusStrip1.SuspendLayout
		Me.SuspendLayout
		'
		'Rojo
		'
		resources.ApplyResources(Me.Rojo, "Rojo")
		Me.Rojo.Name = "Rojo"
		Me.Rojo.TabStop = false
		'
		'Verde
		'
		resources.ApplyResources(Me.Verde, "Verde")
		Me.Verde.Name = "Verde"
		Me.Verde.TabStop = false
		'
		'tabControl1
		'
		Me.tabControl1.Controls.Add(Me.tabPage1)
		Me.tabControl1.Controls.Add(Me.tabPage2)
		resources.ApplyResources(Me.tabControl1, "tabControl1")
		Me.tabControl1.Name = "tabControl1"
		Me.tabControl1.SelectedIndex = 0
		AddHandler Me.tabControl1.SelectedIndexChanged, AddressOf Me.TabControl1SelectedIndexChanged
		'
		'tabPage1
		'
		Me.tabPage1.Controls.Add(Me.chklstNombre)
		Me.tabPage1.Controls.Add(Me.chkList)
		Me.tabPage1.Controls.Add(Me.lstUpdates)
		Me.tabPage1.Controls.Add(Me.btnPass)
		resources.ApplyResources(Me.tabPage1, "tabPage1")
		Me.tabPage1.Name = "tabPage1"
		Me.tabPage1.UseVisualStyleBackColor = true
		'
		'chklstNombre
		'
		Me.chklstNombre.CheckOnClick = true
		Me.chklstNombre.FormattingEnabled = true
		resources.ApplyResources(Me.chklstNombre, "chklstNombre")
		Me.chklstNombre.Name = "chklstNombre"
		AddHandler Me.chklstNombre.ItemCheck, AddressOf Me.ChklstNombreItemCheck
		'
		'chkList
		'
		Me.chkList.CheckOnClick = true
		Me.chkList.FormattingEnabled = true
		resources.ApplyResources(Me.chkList, "chkList")
		Me.chkList.Name = "chkList"
		'
		'lstUpdates
		'
		Me.lstUpdates.FormattingEnabled = true
		resources.ApplyResources(Me.lstUpdates, "lstUpdates")
		Me.lstUpdates.Name = "lstUpdates"
		Me.lstUpdates.Sorted = true
		'
		'btnPass
		'
		resources.ApplyResources(Me.btnPass, "btnPass")
		Me.btnPass.Name = "btnPass"
		Me.btnPass.UseVisualStyleBackColor = true
		AddHandler Me.btnPass.Click, AddressOf Me.BtnPassClick
		'
		'tabPage2
		'
		Me.tabPage2.Controls.Add(Me.lblFalta)
		Me.tabPage2.Controls.Add(Me.lblActivo)
		Me.tabPage2.Controls.Add(Me.txtPassword)
		Me.tabPage2.Controls.Add(Me.txtUsuario)
		Me.tabPage2.Controls.Add(Me.txtServidor)
		Me.tabPage2.Controls.Add(Me.label4)
		Me.tabPage2.Controls.Add(Me.label3)
		Me.tabPage2.Controls.Add(Me.label2)
		Me.tabPage2.Controls.Add(Me.label1)
		Me.tabPage2.Controls.Add(Me.UpDn)
		Me.tabPage2.Controls.Add(Me.btnActivo)
		resources.ApplyResources(Me.tabPage2, "tabPage2")
		Me.tabPage2.Name = "tabPage2"
		Me.tabPage2.UseVisualStyleBackColor = true
		'
		'lblFalta
		'
		resources.ApplyResources(Me.lblFalta, "lblFalta")
		Me.lblFalta.Name = "lblFalta"
		'
		'lblActivo
		'
		resources.ApplyResources(Me.lblActivo, "lblActivo")
		Me.lblActivo.Name = "lblActivo"
		'
		'txtPassword
		'
		resources.ApplyResources(Me.txtPassword, "txtPassword")
		Me.txtPassword.Name = "txtPassword"
		'
		'txtUsuario
		'
		resources.ApplyResources(Me.txtUsuario, "txtUsuario")
		Me.txtUsuario.Name = "txtUsuario"
		'
		'txtServidor
		'
		resources.ApplyResources(Me.txtServidor, "txtServidor")
		Me.txtServidor.Name = "txtServidor"
		'
		'label4
		'
		resources.ApplyResources(Me.label4, "label4")
		Me.label4.Name = "label4"
		'
		'label3
		'
		resources.ApplyResources(Me.label3, "label3")
		Me.label3.Name = "label3"
		'
		'label2
		'
		resources.ApplyResources(Me.label2, "label2")
		Me.label2.Name = "label2"
		'
		'label1
		'
		resources.ApplyResources(Me.label1, "label1")
		Me.label1.Name = "label1"
		'
		'UpDn
		'
		resources.ApplyResources(Me.UpDn, "UpDn")
		Me.UpDn.Maximum = New Decimal(New Integer() {170, 0, 0, 0})
		Me.UpDn.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
		Me.UpDn.Name = "UpDn"
		Me.UpDn.Value = New Decimal(New Integer() {1, 0, 0, 0})
		AddHandler Me.UpDn.ValueChanged, AddressOf Me.UpDnValueChanged
		'
		'btnActivo
		'
		resources.ApplyResources(Me.btnActivo, "btnActivo")
		Me.btnActivo.Name = "btnActivo"
		Me.btnActivo.UseVisualStyleBackColor = true
		AddHandler Me.btnActivo.Click, AddressOf Me.BtnActivoClick
		'
		'rtTexto
		'
		resources.ApplyResources(Me.rtTexto, "rtTexto")
		Me.rtTexto.Name = "rtTexto"
		'
		'btnCerrar
		'
		Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		resources.ApplyResources(Me.btnCerrar, "btnCerrar")
		Me.btnCerrar.Name = "btnCerrar"
		Me.btnCerrar.UseVisualStyleBackColor = true
		AddHandler Me.btnCerrar.Click, AddressOf Me.BtnCerrarClick
		'
		'lstTablas
		'
		Me.lstTablas.FormattingEnabled = true
		resources.ApplyResources(Me.lstTablas, "lstTablas")
		Me.lstTablas.Name = "lstTablas"
		'
		'lstKey
		'
		Me.lstKey.FormattingEnabled = true
		resources.ApplyResources(Me.lstKey, "lstKey")
		Me.lstKey.Name = "lstKey"
		'
		'dataGridView2
		'
		Me.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		resources.ApplyResources(Me.dataGridView2, "dataGridView2")
		Me.dataGridView2.Name = "dataGridView2"
		'
		'dataGridView1
		'
		Me.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		resources.ApplyResources(Me.dataGridView1, "dataGridView1")
		Me.dataGridView1.Name = "dataGridView1"
		'
		'mnuContextual
		'
		Me.mnuContextual.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.restaurarToolStripMenuItem, Me.toolStripMenuItem1, Me.salirToolStripMenuItem})
		Me.mnuContextual.Name = "mnuContextual"
		resources.ApplyResources(Me.mnuContextual, "mnuContextual")
		'
		'restaurarToolStripMenuItem
		'
		Me.restaurarToolStripMenuItem.Name = "restaurarToolStripMenuItem"
		resources.ApplyResources(Me.restaurarToolStripMenuItem, "restaurarToolStripMenuItem")
		'
		'toolStripMenuItem1
		'
		Me.toolStripMenuItem1.Name = "toolStripMenuItem1"
		resources.ApplyResources(Me.toolStripMenuItem1, "toolStripMenuItem1")
		'
		'salirToolStripMenuItem
		'
		Me.salirToolStripMenuItem.Name = "salirToolStripMenuItem"
		resources.ApplyResources(Me.salirToolStripMenuItem, "salirToolStripMenuItem")
		'
		'tmTiempo
		'
		Me.tmTiempo.Interval = 1000000
		AddHandler Me.tmTiempo.Tick, AddressOf Me.TmTiempoTick
		'
		'statusStrip1
		'
		Me.statusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolText, Me.toolStatus})
		resources.ApplyResources(Me.statusStrip1, "statusStrip1")
		Me.statusStrip1.Name = "statusStrip1"
		Me.statusStrip1.SizingGrip = false
		'
		'toolText
		'
		resources.ApplyResources(Me.toolText, "toolText")
		Me.toolText.Name = "toolText"
		'
		'toolStatus
		'
		resources.ApplyResources(Me.toolStatus, "toolStatus")
		Me.toolStatus.Name = "toolStatus"
		'
		'tmpFalta
		'
		Me.tmpFalta.Interval = 1000
		AddHandler Me.tmpFalta.Tick, AddressOf Me.TmpFaltaTick
		'
		'frmUpdCatalogos
		'
		resources.ApplyResources(Me, "$this")
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.ControlBox = false
		Me.Controls.Add(Me.statusStrip1)
		Me.Controls.Add(Me.Rojo)
		Me.Controls.Add(Me.Verde)
		Me.Controls.Add(Me.tabControl1)
		Me.Controls.Add(Me.rtTexto)
		Me.Controls.Add(Me.btnCerrar)
		Me.Controls.Add(Me.lstTablas)
		Me.Controls.Add(Me.lstKey)
		Me.Controls.Add(Me.dataGridView2)
		Me.Controls.Add(Me.dataGridView1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MaximizeBox = false
		Me.MinimizeBox = false
		Me.Name = "frmUpdCatalogos"
		Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
		AddHandler Load, AddressOf Me.FrmUpdCatalogosLoad
		AddHandler ParentChanged, AddressOf Me.FrmUpdCatalogosParentChanged
		CType(Me.Rojo,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.Verde,System.ComponentModel.ISupportInitialize).EndInit
		Me.tabControl1.ResumeLayout(false)
		Me.tabPage1.ResumeLayout(false)
		Me.tabPage2.ResumeLayout(false)
		Me.tabPage2.PerformLayout
		CType(Me.UpDn,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.dataGridView2,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.dataGridView1,System.ComponentModel.ISupportInitialize).EndInit
		Me.mnuContextual.ResumeLayout(false)
		Me.statusStrip1.ResumeLayout(false)
		Me.statusStrip1.PerformLayout
		Me.ResumeLayout(false)
		Me.PerformLayout
	End Sub
	Private chklstNombre As System.Windows.Forms.CheckedListBox
	Private tmpFalta As System.Windows.Forms.Timer
	Private lblFalta As System.Windows.Forms.Label
	Private lblActivo As System.Windows.Forms.Label
	Private toolStatus As System.Windows.Forms.ToolStripStatusLabel
	Private toolText As System.Windows.Forms.ToolStripStatusLabel
	Private statusStrip1 As System.Windows.Forms.StatusStrip
	Private tmTiempo As System.Windows.Forms.Timer
	Private salirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
	Private restaurarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private mnuContextual As System.Windows.Forms.ContextMenuStrip
	Private dataGridView1 As System.Windows.Forms.DataGridView
	Private dataGridView2 As System.Windows.Forms.DataGridView
	Private lstKey As System.Windows.Forms.ListBox
	Private lstTablas As System.Windows.Forms.ListBox
	Private btnCerrar As System.Windows.Forms.Button
	Private rtTexto As System.Windows.Forms.RichTextBox
	Private btnActivo As System.Windows.Forms.Button
	Private UpDn As System.Windows.Forms.NumericUpDown
	Private label1 As System.Windows.Forms.Label
	Private label2 As System.Windows.Forms.Label
	Private label3 As System.Windows.Forms.Label
	Private label4 As System.Windows.Forms.Label
	Private txtServidor As System.Windows.Forms.TextBox
	Private txtUsuario As System.Windows.Forms.TextBox
	Private txtPassword As System.Windows.Forms.TextBox
	Private tabPage2 As System.Windows.Forms.TabPage
	Private btnPass As System.Windows.Forms.Button
	Private lstUpdates As System.Windows.Forms.ListBox
	Private chkList As System.Windows.Forms.CheckedListBox
	Private tabPage1 As System.Windows.Forms.TabPage
	Private tabControl1 As System.Windows.Forms.TabControl
	Private Verde As System.Windows.Forms.PictureBox
	Private Rojo As System.Windows.Forms.PictureBox
	'
	Private custDS As DataSet
	Private custDST As DataSet
	Private vExisteEnCatalogo As Boolean
	Private nError As Integer
	Private vTipoLlave As String
	
	Private Shared connOLE As New OleDb.OleDbConnection
	Private Shared xjmt    As New OracleConnection()
	'
	Public vHora As DateTime
	Public dHora As DateTime
End Class
