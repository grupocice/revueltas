﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 16/02/2017
' Hora: 10:13 a. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports System.Data.OleDb
Imports Oracle.DataAccess.Client
Imports System.Data
Imports System.Configuration
'
Public Partial Class frmUpdCatalogos
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		Me.InitializeComponent()
		
		'
		' TODO : Add constructor code after InitializeComponents
		'
	End Sub
	'
	Private Sub InsertaDatosADMI(ByVal pTabla As String, ByVal pLlave As String)
		'Dim sCabeceraInsert As String '= "insert into " + pTabla + "(" + strExport.TrimEnd(","c) + ") Values ("
		Dim ddataRow As DataRow
		'Dim dfieldRow As DataRow
		Dim nRenglon As Integer
		Dim vCampoValor As String
		Dim vQuery As String
		Dim vFecha As String
		Dim dFecha As Date
		Dim vTipo As String
		'
		'Dim sCadena() As String = {"varchar", "char", "bit"} ' Son Cadenas y requieren chr(39)
		Dim nCantidad As Integer = custDS.Tables(0).Rows.Count
		Dim nColumnas As Integer = custDST.Tables(0).Rows.Count
		If nCantidad > 0 Then
			For nRenglon = 0 To nCantidad - 1
				ddataRow = custDS.Tables(0).Rows(nRenglon)
				vCampoValor = custDS.Tables(0).Rows(nRenglon).Item(pLlave).ToString
				For i = 0 To nColumnas - 1
					vTipo = custDST.Tables(0).Rows(i).Item(0).ToString
					If pLlave.ToUpper = vTipo.ToUpper Then
						vTipoLlave = custDST.Tables(0).Rows(i).Item(1).ToString
						Exit For
		    		End If
		    	Next
				'
				Select Case vTipoLlave
					Case "varchar", "char", "bit", "long varchar"
						vQuery = "SELECT count(" + pLlave + ") as Cantidad FROM " + pTabla + " where " + pLlave + " = '" + vCampoValor + "'"
						getDatoADMI(vQuery)
					Case Else
						vQuery = "SELECT count(" + pLlave + ") as Cantidad FROM " + pTabla + " where " + pLlave + " = " + vCampoValor
						getDatoADMI(vQuery)
				End Select
				'
		    	If vExisteEnCatalogo Then ' Quiere decir que encontro el registro del renglon
		    		vQuery = Chr(13) + "Update " + pTabla + " set "
		    		For i = 0 To nColumnas - 1 
		    			Select Case custDST.Tables(0).Rows(i).Item(1).ToString
		    				Case "varchar", "char", "long varchar"
		    					vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = " + Chr(39) + custDS.Tables(0).Rows(nRenglon).Item(i).ToString + Chr(39) + ", "
		    				Case "bit"
		    					If custDS.Tables(0).Rows(nRenglon).Item(i).ToString.ToUpper = "TRUE" Then
		    						vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = '1', "
		    					Else
		    						vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = '0', "
		    					End If
		    				Case "timestamp"
		    					vFecha = custDS.Tables(0).Rows(nRenglon).Item(i).ToString
								If Trim(vFecha) <> vbNullString Then
									dFecha = Convert.ToDateTime(custDS.Tables(0).Rows(nRenglon).Item(i).ToString)
									vFecha = dFecha.ToString("dd/MM/yyyy HH:mm:ss")
									vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + "), "
									'vPes_FecHorSeg = "TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + ")"
								Else
									vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = NULL, "
								End If
		    				Case Else
		    					If custDS.Tables(0).Rows(nRenglon).Item(i).ToString = "" Then
		    						vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = 0, "
		    					Else
		    						vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = " + custDS.Tables(0).Rows(nRenglon).Item(i).ToString + ", "
		    					End If
		    			 		'vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = " + custDS.Tables(0).Rows(nRenglon).Item(i).ToString + ", "
		    			End Select
		    		Next
		    		vQuery = vQuery.TrimEnd(" "c)
		    		vQuery = vQuery.TrimEnd(","c)
		    		Select Case vTipoLlave
						Case "varchar", "char", "bit", "long varchar"
							vQuery = vQuery + " Where " + pLlave + " = '" + vCampoValor + "'" + Chr(13)
						Case Else
							vQuery = vQuery + " Where " + pLlave + " = " + vCampoValor + Chr(13)
					End Select
		    	Else
		    		vQuery = "Insert Into " + pTabla + " values("
		    		For i = 0 To nColumnas - 1
		    			 Select Case custDST.Tables(0).Rows(i).Item(1).ToString
		    				Case "varchar", "char", "long varchar"
		    			 		vQuery = vQuery + Chr(39) + custDS.Tables(0).Rows(nRenglon).Item(i).ToString + Chr(39) + ", "
		    			 	Case "bit"
		    					If custDS.Tables(0).Rows(nRenglon).Item(i).ToString.ToUpper = "TRUE" Then
		    						vQuery = vQuery + "'1', "
		    					Else
		    						vQuery = vQuery + "'0', "
		    					End If	
		    				Case "timestamp"
		    					vFecha = custDS.Tables(0).Rows(nRenglon).Item(i).ToString
								If Trim(vFecha) <> vbNullString Then
									dFecha = Convert.ToDateTime(custDS.Tables(0).Rows(nRenglon).Item(i).ToString)
									vFecha = dFecha.ToString("dd/MM/yyyy HH:mm:ss")
									vQuery = vQuery + "TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + "), "
									'vPes_FecHorSeg = "TO_DATE(" + Chr(39) + vFecha + Chr(39) + ", " + Chr(39) + "DD/MM/YYYY HH24:MI:SS" + Chr(39) + ")"
								Else
									vQuery = vQuery + custDST.Tables(0).Rows(i).Item(0).ToString + " = NULL, "
								End If	
		    				Case Else
		    					If custDS.Tables(0).Rows(nRenglon).Item(i).ToString = vbNullString Then
		    						vQuery = vQuery + "NULL, "
		    					Else
		    						vQuery = vQuery + custDS.Tables(0).Rows(nRenglon).Item(i).ToString + ", "
		    					End If
		    			 End Select
		    		Next
		    		vQuery = vQuery.TrimEnd(" "c)
		    		vQuery = vQuery.TrimEnd(","c)
		    		vQuery = vQuery + ")" + Chr(13)
		    	End If
		    	rtTexto.Text += vQuery.TrimEnd
		    	EjecutaJMT(vQuery.TrimEnd)
			Next
			'
		End If
		'
	End Sub
	'
	Sub EjecutaJMT(ByVal sInstruccion As String)
		'
		Try
			xjmt.Open
			Dim dExecute As OracleDataAdapter = New OracleDataAdapter(sInstruccion, xjmt)
			dExecute.SelectCommand.ExecuteNonQuery()
		Catch ex As Exception
			 MessageBox.Show("Error:" & vbCrLf & ex.Message)
		End Try	
		xjmt.Close
		'
    End Sub
	'
	Sub getDatoADMI(ByVal selectCommand As String)
		'
		nError = 0
		Dim custDSM As DataSet = New DataSet
		Dim custDA As OracleDataAdapter = New OracleDataAdapter
		Dim selectCMD As OracleCommand = New OracleCommand(selectCommand, xjmt)
		'
		Try
			selectCMD.CommandTimeout = 30
	        custDA.SelectCommand = selectCMD
	        custDA.Fill(custDSM, "Datos")
	    Catch ex As Exception
			nError = 1
			MessageBox.Show("Error:" & vbCrLf & ex.Message)
		Finally
	        '
	        If nError = 0 Then
		        If custDSM.Tables(0).Rows.Count = 0 Then
		        	vExisteEnCatalogo = False
		        ElseIf custDSM.Tables(0).Rows(0)("Cantidad").ToString = "0" Then
		        	vExisteEnCatalogo = False
		        Else
		        	vExisteEnCatalogo = True
		        End If
	        Else
	        	vExisteEnCatalogo = False
	        End If
	    End Try
	End Sub
	'
	Sub GetDatosRevuelta(ByVal pTabla As String, ByVal pTablaADMI As String, byval pLlave As String)
		'
		DataGridView1.ReadOnly = False
		DataGridView1.Rows.Clear
		DataGridView1.Columns.Clear
		'
		DataGridView2.ReadOnly = False
		DataGridView2.Rows.Clear
		DataGridView2.Columns.Clear
		'
		Dim ddataRow As DataRow
		custDS = New DataSet
		custDST = New DataSet
		'
		Dim vTabla As String = pTabla
		Dim selectCommand As String = "select * from dba." + vTabla
		Dim selectTypeCMD As String = "SELECT cname as Nombre, coltype as Tipo, nulls as PermiteNull, length as Largo, syslength as LargoDec, in_primary_key, colno as secuencia, remarks as Comentarios FROM sys.syscolumns where tname = '" + vTabla + "'"
		'
		Try
			Dim selectCMD As OleDb.OleDbCommand = New OleDb.OleDbCommand(selectCommand, connOLE)
			Dim custDA As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
			'
			selectCMD.CommandTimeout = 30
        	custDA.SelectCommand = selectCMD
        	custDA.Fill(custDS, "Datos")
        	'
        	For i = 0 To custDS.Tables(0).Columns.Count - 1
            	dataGridView1.Columns.Add(custDS.Tables(0).Columns(i).ColumnName, custDS.Tables(0).Columns(i).ColumnName)
			Next
        	'
        	Dim nCantidad As Integer = custDS.Tables(0).Rows.Count
        	If nCantidad > 0 Then
				For i = 0 To nCantidad - 1
				    ddataRow = custDS.Tables(0).Rows(i)
				    DataGridView1.Rows.Add(ddataRow.ItemArray)
				Next
        	End If
        	'
        	dataGridView1.DefaultCellStyle.WrapMode = DataGridViewTriState.[True]
        	dataGridView1.Refresh
        	'
        	selectCMD  = New OleDb.OleDbCommand(selectTypeCMD, connOLE)
			custDA = New OleDb.OleDbDataAdapter
			'
			selectCMD.CommandTimeout = 30
        	custDA.SelectCommand = selectCMD
        	custDA.Fill(custDST, "Datos")
        	'
        	For i = 0 To custDST.Tables(0).Columns.Count - 1
            	dataGridView2.Columns.Add(custDST.Tables(0).Columns(i).ColumnName, custDST.Tables(0).Columns(i).ColumnName)
			Next
        	'
        	nCantidad = custDST.Tables(0).Rows.Count
        	If nCantidad > 0 Then
				For i = 0 To nCantidad - 1
				    ddataRow = custDST.Tables(0).Rows(i)
				    DataGridView2.Rows.Add(ddataRow.ItemArray)
				    '
				    If custDST.Tables(0).Rows(i).Item(0).ToString = pLlave Then
				    	vTipoLlave = custDST.Tables(0).Rows(i).Item(1).ToString
				    End If
				Next
        	End If
        	dataGridView2.DefaultCellStyle.WrapMode = DataGridViewTriState.[True]
        	'
        Catch ex As Exception
			MessageBox.Show("Error" & vbCrLf & ex.Message)
        Finally
        	dataGridView1.Refresh
        	dataGridView2.Refresh
        	dataGridView1.ReadOnly = True
        	dataGridView2.ReadOnly = True
        End Try
	End Sub
	'
	Sub GetDataJMT(ByVal selectCommand As String)
		'
		Dim Cadena As String
		custDS = New Dataset
		Dim selectCMD As OracleCommand = New OracleCommand(selectCommand, xjmt)
		Dim custDA As OracleDataAdapter = New OracleDataAdapter
		'
		Cadena = xjmt.ConnectionString
		selectCMD.CommandTimeout = 30
        custDA.SelectCommand = selectCMD
        custDA.Fill(custDS, "Datos")
        '
        For i = 0 To custDS.Tables(0).Rows.Count - 1
        	chkList.Items.Add(custDS.Tables(0).Rows(i).Item(0).ToString, False)
        	lstTablas.Items.Add(custDS.Tables(0).Rows(i).Item(1).ToString)
        	lstKey.Items.Add(custDS.Tables(0).Rows(i).Item(2).ToString)
        	chklstNombre.Items.Add(custDS.Tables(0).Rows(i).Item(3).ToString, False)
        Next
        '
    End Sub
	'
	Sub ConectarOLE()
		Dim Cadena As String
		Cadena = System.Configuration.ConfigurationManager.ConnectionStrings("RevueltaSIP").ConnectionString
		
		Cadena = String.Format(Cadena, My.Settings.RevueltaServer, My.Settings.RevueltaUsuario, My.Settings.RevueltaPass)
		connOLE = New OleDb.OleDbConnection(Cadena)
    End Sub
	'
	 Sub ConectarseJMT()
		Dim cadena As String
		Cadena = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
		'
		Dim oradb As String 
		oradb = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
		oradb = String.Format(oradb, My.Settings.IP, My.Settings.Usuario, My.Settings.Contrasena)
		xjmt.ConnectionString = oradb
        '
	 End Sub
	 
	 Sub FrmUpdCatalogosLoad(sender As Object, e As EventArgs)
	 	'If MainForm.vCatalogos = False Then
	 	'Dim r As Rectangle = MainForm.Size
		'
       	Me.Location = New Point(MainForm.Size.Width - (Me.Width + 30), MainForm.Size.Height - (Me.Height + 90))
	 	'
		 	Dim vQuery As String
			'
			lstUpdates.Items.Clear
			ConectarseJMT()
			'
			vQuery = "Select CTRTABLAREVUELTA as Catalogo, CTRTABLAADMI as Tabla_ADMI, ctrcampoBusq as Clave, CTRNOMBRE AS DESCRIPCION from tblCatalogos_Revuelta where CtrEstado = 'VIGENTE'"
			GetDataJMT(vQuery)
			'
			tmTiempo.Enabled = False
			btnActivo.Text = "&Activar"
			toolStatus.Text = "Status: Inactivo"
			toolStatus.Image = Rojo.Image
			'
			'Notify1.Visible = False
			MainForm.vCatalogos = True
		'End If
	End Sub
	 
	Sub BtnPassClick(sender As Object, e As EventArgs)
		' Poner el valor de las tablas seleccionadas
		Dim idx As Integer = 0
		lstUpdates.Items.Clear
		For idx = 0 To chkList.Items.Count - 1
			If chkList.GetItemChecked(idx)  Then
				' Buscar los valores en RevueltaSIP y pasarlos a ADMI
				lstUpdates.Items.Add(chkList.Items(idx).ToString)
				ConectarOLE()
				GetDatosRevuelta(chkList.Items(idx).ToString, lstTablas.Items(idx).ToString, lstKey.Items(idx).ToString)
				InsertaDatosADMI(lstTablas.Items(idx).ToString, lstKey.Items(idx).ToString)
			End If
		Next
		'
		toolText.Text = "Ultima Ejecución: " + Now.ToString
		'
	End Sub
	
	Sub TmTiempoTick(sender As Object, e As EventArgs)
		BtnPassClick(sender, e)
		Dim HoraActual As DateTime = DateTime.Now
		dHora = HoraActual.AddHours(UpDn.Value)
		'dHora = HoraActual.AddMinutes(UpDn.Value)
	End Sub
	
	Sub UpDnValueChanged(sender As Object, e As EventArgs)
		'
		tmTiempo.Enabled = False
		btnActivo.Text = "&Activar"
		toolStatus.Text = "Status: Inactivo"
		toolStatus.Image = Rojo.Image
		'
		tmpFalta.Enabled = False
		lblFalta.Text = vbNullString
	End Sub
	
	Sub TabControl1SelectedIndexChanged(sender As Object, e As EventArgs)
		If tabControl1.SelectedIndex = 1 Then
			' , , )
			txtServidor.Text = My.Settings.IP
			txtUsuario.Text = My.Settings.Usuario
			txtPassword.Text = My.Settings.Contrasena
		End If
	End Sub
	
	Sub BtnCerrarClick(sender As Object, e As EventArgs)
		'tmTiempo.Enabled = False
		Me.Hide 'Close
	End Sub
	
	Sub BtnActivoClick(sender As Object, e As EventArgs)
		If UpDn.Value > 0 Then
			vHora = DateTime.Now
			lblActivo.Text = "Activado a las " + Format("HH:mm", vHora).ToString
			tmTiempo.Interval = (UpDn.Value * 1000) * 60 * 60
			dHora = vHora.AddHours(UpDn.Value)
			'tmTiempo.Interval = (UpDn.Value * 1000) * 60 '* 60
			'dHora = vHora.AddMinutes(UpDn.Value)
			tmTiempo.Enabled = Not tmTiempo.Enabled
			If tmTiempo.Enabled Then
				btnActivo.Text = "&Desactivar"
				toolStatus.Text = "Status: Activo"
				toolStatus.Image = Verde.Image
				'
				tmpFalta.Enabled = True
				'
				My.Settings.IP = txtServidor.Text
				My.Settings.Usuario  = txtUsuario.Text
				My.Settings.Contrasena = txtPassword.Text
				'
			Else
				lblActivo.Text = vbNullString
				btnActivo.Text = "&Activar"
				toolStatus.Text = "Status: Inactivo"
				toolStatus.Image = Rojo.Image
				'
				tmpFalta.Enabled = False
				lblFalta.Text = vbNullString
			End If
		End If
	End Sub
	
	Sub FrmUpdCatalogosParentChanged(sender As Object, e As EventArgs)
		'Location = New Point(MainForm.Size.Width - (Width + 30), MainForm.Size.Height - (Height + 90))
	End Sub
	
	Sub TmpFaltaTick(sender As Object, e As EventArgs)
		'
		Dim vFalta As Datetime = DateTime.Now
		Dim diferencia As TimeSpan = dHora.Subtract(vFalta)
		'
		lblFalta.Text = String.Format("{00} {1:00}:{2:00}:{3:00}", _
            diferencia.Days, diferencia.Hours, diferencia.Minutes, diferencia.Seconds)
        'lblFalta.Text = Format("HH:mm", diferencia).ToString
		' tmTiempo.Interval
		'
	End Sub
	
	Sub ChklstNombreItemCheck(sender As Object, e As ItemCheckEventArgs)
		If chklstNombre.GetItemChecked(e.Index) = False Then
			chkList.SetItemChecked(e.Index, True)
		Else
			chkList.SetItemChecked(e.Index, False)
		End If
	End Sub
End Class
