﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 12/12/2016
' Hora: 04:30 p. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports Oracle.DataAccess.Client
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
'
Partial Class frmConfiguracion
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConfiguracion))
		Me.label3 = New System.Windows.Forms.Label()
		Me.btnTest = New System.Windows.Forms.Button()
		Me.txtServer = New System.Windows.Forms.TextBox()
		Me.label2 = New System.Windows.Forms.Label()
		Me.label1 = New System.Windows.Forms.Label()
		Me.txtPassword = New System.Windows.Forms.TextBox()
		Me.txtUsuario = New System.Windows.Forms.TextBox()
		Me.btnAccept = New System.Windows.Forms.Button()
		Me.btnCancel = New System.Windows.Forms.Button()
		Me.groupBox1 = New System.Windows.Forms.GroupBox()
		Me.label6 = New System.Windows.Forms.Label()
		Me.label5 = New System.Windows.Forms.Label()
		Me.label4 = New System.Windows.Forms.Label()
		Me.txtPassRev = New System.Windows.Forms.TextBox()
		Me.txtUsuarioRev = New System.Windows.Forms.TextBox()
		Me.txtServerRev = New System.Windows.Forms.TextBox()
		Me.btnTestRev = New System.Windows.Forms.Button()
		Me.groupBox2 = New System.Windows.Forms.GroupBox()
		Me.groupBox1.SuspendLayout
		Me.groupBox2.SuspendLayout
		Me.SuspendLayout
		'
		'label3
		'
		Me.label3.Location = New System.Drawing.Point(10, 15)
		Me.label3.Name = "label3"
		Me.label3.Size = New System.Drawing.Size(96, 20)
		Me.label3.TabIndex = 7
		Me.label3.Text = "Servidor"
		Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'btnTest
		'
		Me.btnTest.Location = New System.Drawing.Point(115, 61)
		Me.btnTest.Name = "btnTest"
		Me.btnTest.Size = New System.Drawing.Size(101, 37)
		Me.btnTest.TabIndex = 3
		Me.btnTest.Text = "Probar conexion"
		Me.btnTest.UseVisualStyleBackColor = true
		AddHandler Me.btnTest.Click, AddressOf Me.BtnTestClick
		'
		'txtServer
		'
		Me.txtServer.Location = New System.Drawing.Point(9, 35)
		Me.txtServer.Name = "txtServer"
		Me.txtServer.Size = New System.Drawing.Size(97, 20)
		Me.txtServer.TabIndex = 0
		'
		'label2
		'
		Me.label2.Location = New System.Drawing.Point(246, 16)
		Me.label2.Name = "label2"
		Me.label2.Size = New System.Drawing.Size(66, 21)
		Me.label2.TabIndex = 4
		Me.label2.Text = "Contraseña"
		Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'label1
		'
		Me.label1.Location = New System.Drawing.Point(138, 15)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(66, 21)
		Me.label1.TabIndex = 3
		Me.label1.Text = "Usuario"
		Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtPassword
		'
		Me.txtPassword.Location = New System.Drawing.Point(225, 35)
		Me.txtPassword.Name = "txtPassword"
		Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
		Me.txtPassword.Size = New System.Drawing.Size(101, 20)
		Me.txtPassword.TabIndex = 2
		'
		'txtUsuario
		'
		Me.txtUsuario.Location = New System.Drawing.Point(115, 35)
		Me.txtUsuario.Name = "txtUsuario"
		Me.txtUsuario.Size = New System.Drawing.Size(101, 20)
		Me.txtUsuario.TabIndex = 1
		'
		'btnAccept
		'
		Me.btnAccept.Location = New System.Drawing.Point(30, 262)
		Me.btnAccept.Name = "btnAccept"
		Me.btnAccept.Size = New System.Drawing.Size(84, 43)
		Me.btnAccept.TabIndex = 10
		Me.btnAccept.Text = "&Guardar"
		Me.btnAccept.UseVisualStyleBackColor = true
		AddHandler Me.btnAccept.Click, AddressOf Me.BtnAcceptClick
		'
		'btnCancel
		'
		Me.btnCancel.Location = New System.Drawing.Point(233, 262)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(84, 43)
		Me.btnCancel.TabIndex = 11
		Me.btnCancel.Text = "&Cancelar"
		Me.btnCancel.UseVisualStyleBackColor = true
		AddHandler Me.btnCancel.Click, AddressOf Me.BtnCancelClick
		'
		'groupBox1
		'
		Me.groupBox1.Controls.Add(Me.label6)
		Me.groupBox1.Controls.Add(Me.label5)
		Me.groupBox1.Controls.Add(Me.label4)
		Me.groupBox1.Controls.Add(Me.txtPassRev)
		Me.groupBox1.Controls.Add(Me.txtUsuarioRev)
		Me.groupBox1.Controls.Add(Me.txtServerRev)
		Me.groupBox1.Controls.Add(Me.btnTestRev)
		Me.groupBox1.Location = New System.Drawing.Point(8, 133)
		Me.groupBox1.Name = "groupBox1"
		Me.groupBox1.Size = New System.Drawing.Size(331, 111)
		Me.groupBox1.TabIndex = 5
		Me.groupBox1.TabStop = false
		Me.groupBox1.Text = " Bascula Revuelta "
		'
		'label6
		'
		Me.label6.Location = New System.Drawing.Point(242, 20)
		Me.label6.Name = "label6"
		Me.label6.Size = New System.Drawing.Size(70, 15)
		Me.label6.TabIndex = 6
		Me.label6.Text = "Contraseña"
		'
		'label5
		'
		Me.label5.Location = New System.Drawing.Point(142, 22)
		Me.label5.Name = "label5"
		Me.label5.Size = New System.Drawing.Size(62, 13)
		Me.label5.TabIndex = 5
		Me.label5.Text = "Usuario"
		'
		'label4
		'
		Me.label4.Location = New System.Drawing.Point(29, 22)
		Me.label4.Name = "label4"
		Me.label4.Size = New System.Drawing.Size(58, 13)
		Me.label4.TabIndex = 4
		Me.label4.Text = "Servidor"
		'
		'txtPassRev
		'
		Me.txtPassRev.Location = New System.Drawing.Point(225, 38)
		Me.txtPassRev.Name = "txtPassRev"
		Me.txtPassRev.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
		Me.txtPassRev.Size = New System.Drawing.Size(101, 20)
		Me.txtPassRev.TabIndex = 8
		'
		'txtUsuarioRev
		'
		Me.txtUsuarioRev.Location = New System.Drawing.Point(115, 38)
		Me.txtUsuarioRev.Name = "txtUsuarioRev"
		Me.txtUsuarioRev.Size = New System.Drawing.Size(101, 20)
		Me.txtUsuarioRev.TabIndex = 7
		'
		'txtServerRev
		'
		Me.txtServerRev.Location = New System.Drawing.Point(9, 38)
		Me.txtServerRev.Name = "txtServerRev"
		Me.txtServerRev.Size = New System.Drawing.Size(97, 20)
		Me.txtServerRev.TabIndex = 6
		'
		'btnTestRev
		'
		Me.btnTestRev.Location = New System.Drawing.Point(115, 66)
		Me.btnTestRev.Name = "btnTestRev"
		Me.btnTestRev.Size = New System.Drawing.Size(101, 35)
		Me.btnTestRev.TabIndex = 9
		Me.btnTestRev.Text = "Conexion Bascula"
		Me.btnTestRev.UseVisualStyleBackColor = true
		AddHandler Me.btnTestRev.Click, AddressOf Me.BtnTestRevClick
		'
		'groupBox2
		'
		Me.groupBox2.Controls.Add(Me.btnTest)
		Me.groupBox2.Controls.Add(Me.txtUsuario)
		Me.groupBox2.Controls.Add(Me.label1)
		Me.groupBox2.Controls.Add(Me.label3)
		Me.groupBox2.Controls.Add(Me.txtPassword)
		Me.groupBox2.Controls.Add(Me.txtServer)
		Me.groupBox2.Controls.Add(Me.label2)
		Me.groupBox2.Location = New System.Drawing.Point(8, 12)
		Me.groupBox2.Name = "groupBox2"
		Me.groupBox2.Size = New System.Drawing.Size(331, 104)
		Me.groupBox2.TabIndex = 0
		Me.groupBox2.TabStop = false
		Me.groupBox2.Text = "Oracle / ADMI"
		'
		'frmConfiguracion
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.ClientSize = New System.Drawing.Size(347, 318)
		Me.ControlBox = false
		Me.Controls.Add(Me.groupBox2)
		Me.Controls.Add(Me.groupBox1)
		Me.Controls.Add(Me.btnAccept)
		Me.Controls.Add(Me.btnCancel)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
		Me.KeyPreview = true
		Me.MaximizeBox = false
		Me.MinimizeBox = false
		Me.Name = "frmConfiguracion"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Configuracion Para Envio"
		AddHandler Load, AddressOf Me.FrmConfiguracionLoad
		AddHandler KeyUp, AddressOf Me.FrmConfiguracionKeyUp
		Me.groupBox1.ResumeLayout(false)
		Me.groupBox1.PerformLayout
		Me.groupBox2.ResumeLayout(false)
		Me.groupBox2.PerformLayout
		Me.ResumeLayout(false)
	End Sub
	Private groupBox2 As System.Windows.Forms.GroupBox
	Private btnTestRev As System.Windows.Forms.Button
	Public txtServerRev As System.Windows.Forms.TextBox
	Public txtUsuarioRev As System.Windows.Forms.TextBox
	Public txtPassRev As System.Windows.Forms.TextBox
	Private label4 As System.Windows.Forms.Label
	Private label5 As System.Windows.Forms.Label
	Private label6 As System.Windows.Forms.Label
	Private groupBox1 As System.Windows.Forms.GroupBox
	Private btnCancel As System.Windows.Forms.Button
	Private btnAccept As System.Windows.Forms.Button
	Public txtUsuario As System.Windows.Forms.TextBox
	Public txtPassword As System.Windows.Forms.TextBox
	Private label1 As System.Windows.Forms.Label
	Private label2 As System.Windows.Forms.Label
	Private btnTest As System.Windows.Forms.Button
	Public txtServer As System.Windows.Forms.TextBox
	Private label3 As System.Windows.Forms.Label
	'
	Private Shared conn    As New OracleConnection()
	'
	Private Shared connSS  As New SqlClient.SqlConnection
	Private Shared connOLE As New OleDb.OleDbConnection
	'
	Private Shared connGrd As New OleDb.OleDbConnection 'SqlClient.SqlConnection
	Private Shared xjmt    As New OracleConnection()
	'
End Class
