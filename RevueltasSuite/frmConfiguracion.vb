﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 12/12/2016
' Hora: 04:30 p. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Data
'
Public Partial Class frmConfiguracion
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		Me.InitializeComponent()
		
		'
		' TODO : Add constructor code after InitializeComponents
		'
	End Sub
	'
	Public Sub ConectarseJMT()
		Dim cadena As String
		Cadena = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
		
		Dim oradb As String 
		oradb = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
		oradb = String.Format(oradb, My.Settings.IP, My.Settings.Usuario, My.Settings.Contrasena)
		xjmt.ConnectionString = oradb
        '
	End Sub
	'
	Public Sub ConectarOLE()
		Dim Cadena As String
		Cadena = System.Configuration.ConfigurationManager.ConnectionStrings("RevueltaSIP").ConnectionString
		Cadena = String.Format(Cadena, My.Settings.RevueltaServer, My.Settings.RevueltaUsuario, My.Settings.RevueltaPass)
		connOLE = New OleDb.OleDbConnection(Cadena)
    End Sub
    '
	'
	Sub BtnCancelClick(sender As Object, e As EventArgs)
		'
		Me.Close
		'
	End Sub
	
	Sub BtnAcceptClick(sender As Object, e As EventArgs)
       	'	
		My.Settings.IP = Me.txtServer.text
        My.Settings.Usuario = Me.txtUsuario.Text
        My.Settings.Contrasena = txtPassword.Text
        '
        My.Settings.Save
        '
        My.Settings.RevueltaServer = txtServerRev.Text
        My.Settings.RevueltaUsuario = txtUsuarioRev.Text
        My.Settings.RevueltaPass = txtPassRev.Text
        '
        My.Settings.Save
        '
        txtServer.text = ""
        txtUsuario.Text = ""
        txtPassword.Text = ""
        Me.Close		
	End Sub
	
	Sub BtnTestClick(sender As Object, e As EventArgs)
		'
		xjmt.Close
        ' Se debe usar los parametros para intentar hacer una conexion a la base de datos
        If (conn.State = System.Data.ConnectionState.Closed) Or (conn.State = System.Data.ConnectionState.Broken) Then
			ConectarseJMT()
		End If
        '
		Try
			xjmt.Open
			MessageBox.Show("Conexion Realizada")
		Catch ex As Exception
			MessageBox.Show("Fallo la conexion, verifique parametros")
		Finally
			xjmt.Close
		End Try		
	End Sub
	
	Sub FrmConfiguracionLoad(sender As Object, e As EventArgs)
		txtServer.text = My.Settings.IP
        txtUsuario.Text = My.Settings.Usuario
        txtPassword.Text = My.Settings.Contrasena
        '
        txtServerRev.Text = My.Settings.RevueltaServer
        txtUsuarioRev.Text = My.Settings.RevueltaUsuario
        txtPassRev.Text = My.Settings.RevueltaPass
	End Sub
	
	Sub BtnTestRevClick(sender As Object, e As EventArgs)
		'
		connOLE.Close
		My.Settings.RevueltaServer = txtServerRev.Text
        My.Settings.RevueltaUsuario = txtUsuarioRev.Text
        My.Settings.RevueltaPass = txtPassRev.Text
        '
        My.Settings.Save()
        ' Se debe usar los parametros para intentar hacer una conexion a la base de datos
        If (connSS.State = System.Data.ConnectionState.Closed) Or (connSS.State = System.Data.ConnectionState.Broken) Then
        	ConectarOLE()
		End If
		'
		'MessageBox.Show(MainForm.connOLE.ConnectionString)
		Try
			connOLE.Open
			MessageBox.Show("Conexion Realizada")
		Catch ex As Exception
			MessageBox.Show("Fallo la conexion, verifique parametros")
		Finally
			connOLE.Close
		End Try		
	End Sub
	
	Sub FrmConfiguracionKeyUp(sender As Object, e As KeyEventArgs)
		If e.KeyCode = Keys.F12 Then
        '
           If txtPassword.PasswordChar = "*"c Then
        	  txtPassword.PasswordChar = ControlChars.NullChar 
        	  txtPassRev.PasswordChar = ControlChars.NullChar
        	Else
        	  txtPassword.PasswordChar = "*"c
        	  txtPassRev.PasswordChar = "*"c
        	End If
		ElseIf e.KeyCode = Keys.Escape Then
			BtnCancelClick(sender, e)
        End If
	End Sub
End Class
