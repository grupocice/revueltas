﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 14/02/2017
' Hora: 03:06 p. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'

Partial Class MainForm
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
		Me.mntCalendario = New System.Windows.Forms.MonthCalendar()
		Me.MnPrincipal = New System.Windows.Forms.MenuStrip()
		Me.tmnArchivo = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
		Me.calendarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
		Me.salirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.editarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.deshacerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.rehacerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
		Me.cortarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
		Me.copiarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
		Me.pegarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
		Me.seleccionartodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.tmnProcesos = New System.Windows.Forms.ToolStripMenuItem()
		Me.consultaDeTicketsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.listaDeTicketsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
		Me.unTicketToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
		Me.modificarTicketsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
		Me.actualizarCatalogosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.herramientasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.personalizarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.opcionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ventanasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.cascadaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.tituloToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.tituloHorizontalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ayudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.contenidoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.índiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.buscarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
		Me.acercadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.consultarTicketsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.listaDeTicketsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
		Me.toolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
		Me.modificarTicketToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.statusStrip1 = New System.Windows.Forms.StatusStrip()
		Me.toolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
		Me.MnPrincipal.SuspendLayout
		Me.statusStrip1.SuspendLayout
		Me.SuspendLayout
		'
		'mntCalendario
		'
		Me.mntCalendario.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
		Me.mntCalendario.Location = New System.Drawing.Point(862, 33)
		Me.mntCalendario.Name = "mntCalendario"
		Me.mntCalendario.TabIndex = 1
		Me.mntCalendario.Visible = false
		'
		'MnPrincipal
		'
		Me.MnPrincipal.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tmnArchivo, Me.editarToolStripMenuItem, Me.tmnProcesos, Me.herramientasToolStripMenuItem, Me.ventanasToolStripMenuItem, Me.ayudaToolStripMenuItem})
		Me.MnPrincipal.Location = New System.Drawing.Point(0, 0)
		Me.MnPrincipal.Name = "MnPrincipal"
		Me.MnPrincipal.Size = New System.Drawing.Size(1119, 24)
		Me.MnPrincipal.TabIndex = 2
		Me.MnPrincipal.Text = "menuStrip1"
		AddHandler Me.MnPrincipal.MouseHover, AddressOf Me.MnPrincipalMouseHover
		'
		'tmnArchivo
		'
		Me.tmnArchivo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripMenuItem6, Me.toolStripSeparator1, Me.calendarioToolStripMenuItem, Me.toolStripMenuItem1, Me.salirToolStripMenuItem})
		Me.tmnArchivo.Name = "tmnArchivo"
		Me.tmnArchivo.Size = New System.Drawing.Size(60, 20)
		Me.tmnArchivo.Text = "Archivo"
		'
		'toolStripMenuItem6
		'
		Me.toolStripMenuItem6.Name = "toolStripMenuItem6"
		Me.toolStripMenuItem6.Size = New System.Drawing.Size(152, 22)
		Me.toolStripMenuItem6.Text = "Configuración"
		Me.toolStripMenuItem6.ToolTipText = "Configuración de Conexiones"
		AddHandler Me.toolStripMenuItem6.Click, AddressOf Me.ToolStripMenuItem6Click
		AddHandler Me.toolStripMenuItem6.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.toolStripMenuItem6.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'toolStripSeparator1
		'
		Me.toolStripSeparator1.Name = "toolStripSeparator1"
		Me.toolStripSeparator1.Size = New System.Drawing.Size(149, 6)
		'
		'calendarioToolStripMenuItem
		'
		Me.calendarioToolStripMenuItem.CheckOnClick = true
		Me.calendarioToolStripMenuItem.Name = "calendarioToolStripMenuItem"
		Me.calendarioToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.calendarioToolStripMenuItem.Text = "Calendario"
		Me.calendarioToolStripMenuItem.ToolTipText = "Muestra/Oculta el calendario"
		AddHandler Me.calendarioToolStripMenuItem.Click, AddressOf Me.CalendarioToolStripMenuItemClick
		AddHandler Me.calendarioToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.calendarioToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'toolStripMenuItem1
		'
		Me.toolStripMenuItem1.Name = "toolStripMenuItem1"
		Me.toolStripMenuItem1.Size = New System.Drawing.Size(149, 6)
		'
		'salirToolStripMenuItem
		'
		Me.salirToolStripMenuItem.Name = "salirToolStripMenuItem"
		Me.salirToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.X),System.Windows.Forms.Keys)
		Me.salirToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.salirToolStripMenuItem.Text = "&Salir"
		Me.salirToolStripMenuItem.ToolTipText = "Sale de la aplicación"
		AddHandler Me.salirToolStripMenuItem.Click, AddressOf Me.SalirToolStripMenuItemClick
		AddHandler Me.salirToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.salirToolStripMenuItem.MouseHover, AddressOf Me.SalirToolStripMenuItemMouseHover
		'
		'editarToolStripMenuItem
		'
		Me.editarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.deshacerToolStripMenuItem, Me.rehacerToolStripMenuItem, Me.toolStripSeparator4, Me.cortarToolStripMenuItem1, Me.copiarToolStripMenuItem1, Me.pegarToolStripMenuItem1, Me.toolStripSeparator5, Me.seleccionartodoToolStripMenuItem})
		Me.editarToolStripMenuItem.Name = "editarToolStripMenuItem"
		Me.editarToolStripMenuItem.Size = New System.Drawing.Size(49, 20)
		Me.editarToolStripMenuItem.Text = "&Editar"
		Me.editarToolStripMenuItem.Visible = false
		'
		'deshacerToolStripMenuItem
		'
		Me.deshacerToolStripMenuItem.Name = "deshacerToolStripMenuItem"
		Me.deshacerToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z),System.Windows.Forms.Keys)
		Me.deshacerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
		Me.deshacerToolStripMenuItem.Text = "&Deshacer"
		'
		'rehacerToolStripMenuItem
		'
		Me.rehacerToolStripMenuItem.Name = "rehacerToolStripMenuItem"
		Me.rehacerToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y),System.Windows.Forms.Keys)
		Me.rehacerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
		Me.rehacerToolStripMenuItem.Text = "&Rehacer"
		'
		'toolStripSeparator4
		'
		Me.toolStripSeparator4.Name = "toolStripSeparator4"
		Me.toolStripSeparator4.Size = New System.Drawing.Size(160, 6)
		'
		'cortarToolStripMenuItem1
		'
		Me.cortarToolStripMenuItem1.Image = CType(resources.GetObject("cortarToolStripMenuItem1.Image"),System.Drawing.Image)
		Me.cortarToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.cortarToolStripMenuItem1.Name = "cortarToolStripMenuItem1"
		Me.cortarToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X),System.Windows.Forms.Keys)
		Me.cortarToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
		Me.cortarToolStripMenuItem1.Text = "Cor&tar"
		'
		'copiarToolStripMenuItem1
		'
		Me.copiarToolStripMenuItem1.Image = CType(resources.GetObject("copiarToolStripMenuItem1.Image"),System.Drawing.Image)
		Me.copiarToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.copiarToolStripMenuItem1.Name = "copiarToolStripMenuItem1"
		Me.copiarToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C),System.Windows.Forms.Keys)
		Me.copiarToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
		Me.copiarToolStripMenuItem1.Text = "&Copiar"
		'
		'pegarToolStripMenuItem1
		'
		Me.pegarToolStripMenuItem1.Image = CType(resources.GetObject("pegarToolStripMenuItem1.Image"),System.Drawing.Image)
		Me.pegarToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.pegarToolStripMenuItem1.Name = "pegarToolStripMenuItem1"
		Me.pegarToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V),System.Windows.Forms.Keys)
		Me.pegarToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
		Me.pegarToolStripMenuItem1.Text = "&Pegar"
		'
		'toolStripSeparator5
		'
		Me.toolStripSeparator5.Name = "toolStripSeparator5"
		Me.toolStripSeparator5.Size = New System.Drawing.Size(160, 6)
		'
		'seleccionartodoToolStripMenuItem
		'
		Me.seleccionartodoToolStripMenuItem.Name = "seleccionartodoToolStripMenuItem"
		Me.seleccionartodoToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
		Me.seleccionartodoToolStripMenuItem.Text = "&Seleccionar todo"
		'
		'tmnProcesos
		'
		Me.tmnProcesos.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.consultaDeTicketsToolStripMenuItem, Me.toolStripMenuItem4, Me.modificarTicketsToolStripMenuItem, Me.toolStripMenuItem7, Me.actualizarCatalogosToolStripMenuItem})
		Me.tmnProcesos.Name = "tmnProcesos"
		Me.tmnProcesos.Size = New System.Drawing.Size(66, 20)
		Me.tmnProcesos.Text = "Procesos"
		'
		'consultaDeTicketsToolStripMenuItem
		'
		Me.consultaDeTicketsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.listaDeTicketsToolStripMenuItem1, Me.toolStripMenuItem5, Me.unTicketToolStripMenuItem})
		Me.consultaDeTicketsToolStripMenuItem.Name = "consultaDeTicketsToolStripMenuItem"
		Me.consultaDeTicketsToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
		Me.consultaDeTicketsToolStripMenuItem.Text = "Consulta de Tickets"
		Me.consultaDeTicketsToolStripMenuItem.ToolTipText = "Consulta de Tickets del Sistema Revueltas"
		'
		'listaDeTicketsToolStripMenuItem1
		'
		Me.listaDeTicketsToolStripMenuItem1.Name = "listaDeTicketsToolStripMenuItem1"
		Me.listaDeTicketsToolStripMenuItem1.Size = New System.Drawing.Size(154, 22)
		Me.listaDeTicketsToolStripMenuItem1.Text = "Lista de Tickets"
		Me.listaDeTicketsToolStripMenuItem1.ToolTipText = "Muestra una lista de Tickets del sistema de Revueltas"
		AddHandler Me.listaDeTicketsToolStripMenuItem1.Click, AddressOf Me.ListaDeTicketsToolStripMenuItem1Click
		AddHandler Me.listaDeTicketsToolStripMenuItem1.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.listaDeTicketsToolStripMenuItem1.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'toolStripMenuItem5
		'
		Me.toolStripMenuItem5.Name = "toolStripMenuItem5"
		Me.toolStripMenuItem5.Size = New System.Drawing.Size(151, 6)
		'
		'unTicketToolStripMenuItem
		'
		Me.unTicketToolStripMenuItem.Name = "unTicketToolStripMenuItem"
		Me.unTicketToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
		Me.unTicketToolStripMenuItem.Text = "Un Ticket"
		Me.unTicketToolStripMenuItem.ToolTipText = "Consulta un ticket del sistema de Revueltas"
		AddHandler Me.unTicketToolStripMenuItem.Click, AddressOf Me.UnTicketToolStripMenuItemClick
		AddHandler Me.unTicketToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.unTicketToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'toolStripMenuItem4
		'
		Me.toolStripMenuItem4.Name = "toolStripMenuItem4"
		Me.toolStripMenuItem4.Size = New System.Drawing.Size(179, 6)
		'
		'modificarTicketsToolStripMenuItem
		'
		Me.modificarTicketsToolStripMenuItem.Name = "modificarTicketsToolStripMenuItem"
		Me.modificarTicketsToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
		Me.modificarTicketsToolStripMenuItem.Text = "Modificar Tickets"
		Me.modificarTicketsToolStripMenuItem.ToolTipText = "Modifica los datos de ticket que se encuentran en APEX"
		AddHandler Me.modificarTicketsToolStripMenuItem.Click, AddressOf Me.ModificarTicketsToolStripMenuItemClick
		AddHandler Me.modificarTicketsToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.modificarTicketsToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'toolStripMenuItem7
		'
		Me.toolStripMenuItem7.Name = "toolStripMenuItem7"
		Me.toolStripMenuItem7.Size = New System.Drawing.Size(179, 6)
		'
		'actualizarCatalogosToolStripMenuItem
		'
		Me.actualizarCatalogosToolStripMenuItem.Name = "actualizarCatalogosToolStripMenuItem"
		Me.actualizarCatalogosToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
		Me.actualizarCatalogosToolStripMenuItem.Text = "Actualizar Catalogos"
		Me.actualizarCatalogosToolStripMenuItem.ToolTipText = "Actualiza los catalogos de apoyo de Revueltas"
		AddHandler Me.actualizarCatalogosToolStripMenuItem.Click, AddressOf Me.ActualizarCatalogosToolStripMenuItemClick
		AddHandler Me.actualizarCatalogosToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.actualizarCatalogosToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'herramientasToolStripMenuItem
		'
		Me.herramientasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.personalizarToolStripMenuItem, Me.opcionesToolStripMenuItem})
		Me.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem"
		Me.herramientasToolStripMenuItem.Size = New System.Drawing.Size(90, 20)
		Me.herramientasToolStripMenuItem.Text = "&Herramientas"
		Me.herramientasToolStripMenuItem.Visible = false
		'
		'personalizarToolStripMenuItem
		'
		Me.personalizarToolStripMenuItem.Name = "personalizarToolStripMenuItem"
		Me.personalizarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.personalizarToolStripMenuItem.Text = "&Personalizar"
		'
		'opcionesToolStripMenuItem
		'
		Me.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem"
		Me.opcionesToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.opcionesToolStripMenuItem.Text = "&Opciones"
		'
		'ventanasToolStripMenuItem
		'
		Me.ventanasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cascadaToolStripMenuItem, Me.tituloToolStripMenuItem, Me.tituloHorizontalToolStripMenuItem})
		Me.ventanasToolStripMenuItem.Name = "ventanasToolStripMenuItem"
		Me.ventanasToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
		Me.ventanasToolStripMenuItem.Text = "Ventana"
		'
		'cascadaToolStripMenuItem
		'
		Me.cascadaToolStripMenuItem.Name = "cascadaToolStripMenuItem"
		Me.cascadaToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
		Me.cascadaToolStripMenuItem.Text = "Cascada"
		Me.cascadaToolStripMenuItem.ToolTipText = "Organiza las ventanas en forma de Lista"
		AddHandler Me.cascadaToolStripMenuItem.Click, AddressOf Me.CascadaToolStripMenuItemClick
		AddHandler Me.cascadaToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.cascadaToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'tituloToolStripMenuItem
		'
		Me.tituloToolStripMenuItem.Name = "tituloToolStripMenuItem"
		Me.tituloToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
		Me.tituloToolStripMenuItem.Text = "Titulo Vertical"
		Me.tituloToolStripMenuItem.ToolTipText = "Organiza las ventanas mostrandolas de forma vertical"
		AddHandler Me.tituloToolStripMenuItem.Click, AddressOf Me.TituloToolStripMenuItemClick
		AddHandler Me.tituloToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.tituloToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'tituloHorizontalToolStripMenuItem
		'
		Me.tituloHorizontalToolStripMenuItem.Name = "tituloHorizontalToolStripMenuItem"
		Me.tituloHorizontalToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
		Me.tituloHorizontalToolStripMenuItem.Text = "Titulo Horizontal"
		Me.tituloHorizontalToolStripMenuItem.ToolTipText = "Organiza las ventanas mostrandolas de forma Horizontal"
		AddHandler Me.tituloHorizontalToolStripMenuItem.Click, AddressOf Me.TituloHorizontalToolStripMenuItemClick
		AddHandler Me.tituloHorizontalToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.tituloHorizontalToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'ayudaToolStripMenuItem
		'
		Me.ayudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.contenidoToolStripMenuItem, Me.índiceToolStripMenuItem, Me.buscarToolStripMenuItem, Me.toolStripSeparator6, Me.acercadeToolStripMenuItem})
		Me.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem"
		Me.ayudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
		Me.ayudaToolStripMenuItem.Text = "Ay&uda"
		'
		'contenidoToolStripMenuItem
		'
		Me.contenidoToolStripMenuItem.Name = "contenidoToolStripMenuItem"
		Me.contenidoToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.contenidoToolStripMenuItem.Text = "&Contenido"
		Me.contenidoToolStripMenuItem.Visible = false
		'
		'índiceToolStripMenuItem
		'
		Me.índiceToolStripMenuItem.Name = "índiceToolStripMenuItem"
		Me.índiceToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.índiceToolStripMenuItem.Text = "Índic&e"
		Me.índiceToolStripMenuItem.Visible = false
		'
		'buscarToolStripMenuItem
		'
		Me.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem"
		Me.buscarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.buscarToolStripMenuItem.Text = "&Buscar"
		Me.buscarToolStripMenuItem.Visible = false
		'
		'toolStripSeparator6
		'
		Me.toolStripSeparator6.Name = "toolStripSeparator6"
		Me.toolStripSeparator6.Size = New System.Drawing.Size(149, 6)
		Me.toolStripSeparator6.Visible = false
		'
		'acercadeToolStripMenuItem
		'
		Me.acercadeToolStripMenuItem.Name = "acercadeToolStripMenuItem"
		Me.acercadeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.acercadeToolStripMenuItem.Text = "&Acerca de..."
		AddHandler Me.acercadeToolStripMenuItem.Click, AddressOf Me.AcercadeToolStripMenuItemClick
		AddHandler Me.acercadeToolStripMenuItem.MouseLeave, AddressOf Me.SalirToolStripMenuItemMouseLeave
		AddHandler Me.acercadeToolStripMenuItem.MouseHover, AddressOf Me.ToolStripMenuItem6MouseHover
		'
		'consultarTicketsToolStripMenuItem
		'
		Me.consultarTicketsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.listaDeTicketsToolStripMenuItem})
		Me.consultarTicketsToolStripMenuItem.Name = "consultarTicketsToolStripMenuItem"
		Me.consultarTicketsToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
		Me.consultarTicketsToolStripMenuItem.Text = "Consultar Tickets"
		AddHandler Me.consultarTicketsToolStripMenuItem.Click, AddressOf Me.ConsultarTicketsToolStripMenuItemClick
		'
		'listaDeTicketsToolStripMenuItem
		'
		Me.listaDeTicketsToolStripMenuItem.Name = "listaDeTicketsToolStripMenuItem"
		Me.listaDeTicketsToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
		Me.listaDeTicketsToolStripMenuItem.Text = "Lista de Tickets"
		AddHandler Me.listaDeTicketsToolStripMenuItem.Click, AddressOf Me.ListaDeTicketsToolStripMenuItemClick
		'
		'toolStripMenuItem2
		'
		Me.toolStripMenuItem2.Name = "toolStripMenuItem2"
		Me.toolStripMenuItem2.Size = New System.Drawing.Size(162, 6)
		'
		'toolStripMenuItem3
		'
		Me.toolStripMenuItem3.Name = "toolStripMenuItem3"
		Me.toolStripMenuItem3.Size = New System.Drawing.Size(162, 6)
		'
		'modificarTicketToolStripMenuItem
		'
		Me.modificarTicketToolStripMenuItem.Name = "modificarTicketToolStripMenuItem"
		Me.modificarTicketToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
		Me.modificarTicketToolStripMenuItem.Text = "Modificar Ticket"
		AddHandler Me.modificarTicketToolStripMenuItem.Click, AddressOf Me.ModificarTicketToolStripMenuItemClick
		'
		'statusStrip1
		'
		Me.statusStrip1.AutoSize = false
		Me.statusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripStatusLabel1})
		Me.statusStrip1.Location = New System.Drawing.Point(0, 573)
		Me.statusStrip1.Name = "statusStrip1"
		Me.statusStrip1.Size = New System.Drawing.Size(1119, 22)
		Me.statusStrip1.SizingGrip = false
		Me.statusStrip1.Stretch = false
		Me.statusStrip1.TabIndex = 4
		Me.statusStrip1.Text = "statusStrip1"
		'
		'toolStripStatusLabel1
		'
		Me.toolStripStatusLabel1.AutoSize = false
		Me.toolStripStatusLabel1.Name = "toolStripStatusLabel1"
		Me.toolStripStatusLabel1.Size = New System.Drawing.Size(800, 17)
		Me.toolStripStatusLabel1.Text = "Suite para Mantenimiento de Datos de Revueltas... "
		Me.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'MainForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
		Me.ClientSize = New System.Drawing.Size(1119, 595)
		Me.Controls.Add(Me.statusStrip1)
		Me.Controls.Add(Me.mntCalendario)
		Me.Controls.Add(Me.MnPrincipal)
		Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
		Me.IsMdiContainer = true
		Me.MainMenuStrip = Me.MnPrincipal
		Me.MinimumSize = New System.Drawing.Size(720, 39)
		Me.Name = "MainForm"
		Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Revueltas Suite"
		AddHandler Load, AddressOf Me.MainFormLoad
		AddHandler ResizeEnd, AddressOf Me.MainFormResizeEnd
		AddHandler SizeChanged, AddressOf Me.MainFormResizeEnd
		AddHandler Resize, AddressOf Me.MainFormResizeEnd
		Me.MnPrincipal.ResumeLayout(false)
		Me.MnPrincipal.PerformLayout
		Me.statusStrip1.ResumeLayout(false)
		Me.statusStrip1.PerformLayout
		Me.ResumeLayout(false)
		Me.PerformLayout
	End Sub
	Private actualizarCatalogosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem7 As System.Windows.Forms.ToolStripSeparator
	Private acercadeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
	Private buscarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private índiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private contenidoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private ayudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private opcionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private personalizarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private herramientasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private seleccionartodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
	Private pegarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
	Private copiarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
	Private cortarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
	Private toolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
	Private rehacerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private deshacerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private editarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
	Private toolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
	Private tituloHorizontalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private tituloToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private cascadaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private ventanasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private unTicketToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem5 As System.Windows.Forms.ToolStripSeparator
	Private modificarTicketsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
	Private listaDeTicketsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
	Private consultaDeTicketsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
	Private listaDeTicketsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	'Private toolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
	'Private toolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
	Private statusStrip1 As System.Windows.Forms.StatusStrip
	Private modificarTicketToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
	'Private enviarTicketToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
	Private consultarTicketsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private salirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
	Private calendarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Private tmnProcesos As System.Windows.Forms.ToolStripMenuItem
	Private tmnArchivo As System.Windows.Forms.ToolStripMenuItem
	Private MnPrincipal As System.Windows.Forms.MenuStrip
	Private mntCalendario As System.Windows.Forms.MonthCalendar
	'
	Public Shared vCatalogos As Boolean = False
	'
End Class
