﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 12/12/2016
' Hora: 06:35 p. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports Oracle.DataAccess.Client
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Net
Imports System.Data.OleDb

Partial Class frmBoletos
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBoletos))
		Dim dataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Me.cmsMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.toolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
		Me.toolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
		Me.btnCerrar = New System.Windows.Forms.Button()
		Me.btnEnvio = New System.Windows.Forms.Button()
		Me.dataGridView1 = New System.Windows.Forms.DataGridView()
		Me.dsEnvios = New System.Data.DataSet()
		Me.cmsMenu.SuspendLayout
		CType(Me.dataGridView1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.dsEnvios,System.ComponentModel.ISupportInitialize).BeginInit
		Me.SuspendLayout
		'
		'cmsMenu
		'
		Me.cmsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripMenuItem1, Me.toolStripSeparator1, Me.toolStripMenuItem2})
		Me.cmsMenu.Name = "contextMenuStrip1"
		Me.cmsMenu.Size = New System.Drawing.Size(143, 54)
		Me.cmsMenu.Text = "Opciones"
		AddHandler Me.cmsMenu.Opening, AddressOf Me.CmsMenuOpening
		'
		'toolStripMenuItem1
		'
		Me.toolStripMenuItem1.AutoToolTip = true
		Me.toolStripMenuItem1.Image = CType(resources.GetObject("toolStripMenuItem1.Image"),System.Drawing.Image)
		Me.toolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.toolStripMenuItem1.Name = "toolStripMenuItem1"
		Me.toolStripMenuItem1.Size = New System.Drawing.Size(142, 22)
		Me.toolStripMenuItem1.Text = "Imprimir"
		Me.toolStripMenuItem1.ToolTipText = "Realiza la impresion del Pesaje"
		Me.toolStripMenuItem1.Visible = false
		'
		'toolStripSeparator1
		'
		Me.toolStripSeparator1.Name = "toolStripSeparator1"
		Me.toolStripSeparator1.Size = New System.Drawing.Size(139, 6)
		Me.toolStripSeparator1.Visible = false
		'
		'toolStripMenuItem2
		'
		Me.toolStripMenuItem2.AutoToolTip = true
		Me.toolStripMenuItem2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
		Me.toolStripMenuItem2.Image = CType(resources.GetObject("toolStripMenuItem2.Image"),System.Drawing.Image)
		Me.toolStripMenuItem2.Name = "toolStripMenuItem2"
		Me.toolStripMenuItem2.Size = New System.Drawing.Size(142, 22)
		Me.toolStripMenuItem2.Text = "Enviar Boleta"
		Me.toolStripMenuItem2.ToolTipText = "Realiza el envio del Pesaje Seleccionado"
		AddHandler Me.toolStripMenuItem2.Click, AddressOf Me.ToolStripMenuItem2Click
		'
		'btnCerrar
		'
		Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
		Me.btnCerrar.Location = New System.Drawing.Point(498, 298)
		Me.btnCerrar.Name = "btnCerrar"
		Me.btnCerrar.Size = New System.Drawing.Size(121, 37)
		Me.btnCerrar.TabIndex = 3
		Me.btnCerrar.Text = "Cerrar"
		Me.btnCerrar.UseVisualStyleBackColor = true
		AddHandler Me.btnCerrar.Click, AddressOf Me.BtnCerrarClick
		'
		'btnEnvio
		'
		Me.btnEnvio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
		Me.btnEnvio.Location = New System.Drawing.Point(15, 298)
		Me.btnEnvio.Name = "btnEnvio"
		Me.btnEnvio.Size = New System.Drawing.Size(112, 36)
		Me.btnEnvio.TabIndex = 2
		Me.btnEnvio.Text = "Enviar"
		Me.btnEnvio.UseVisualStyleBackColor = true
		AddHandler Me.btnEnvio.Click, AddressOf Me.ToolStripMenuItem2Click
		'
		'dataGridView1
		'
		Me.dataGridView1.AllowUserToAddRows = false
		Me.dataGridView1.AllowUserToDeleteRows = false
		Me.dataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
						Or System.Windows.Forms.AnchorStyles.Left)  _
						Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
		Me.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
		Me.dataGridView1.BackgroundColor = System.Drawing.Color.White
		Me.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.dataGridView1.ContextMenuStrip = Me.cmsMenu
		dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
		dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
		dataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
		dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
		dataGridViewCellStyle1.Format = "N2"
		dataGridViewCellStyle1.NullValue = Nothing
		dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
		dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
		dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
		Me.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1
		Me.dataGridView1.Location = New System.Drawing.Point(1, 0)
		Me.dataGridView1.MultiSelect = false
		Me.dataGridView1.Name = "dataGridView1"
		Me.dataGridView1.RowTemplate.Height = 30
		Me.dataGridView1.Size = New System.Drawing.Size(639, 270)
		Me.dataGridView1.TabIndex = 4
		AddHandler Me.dataGridView1.CellClick, AddressOf Me.DataGridView1CellClick
		'
		'dsEnvios
		'
		Me.dsEnvios.DataSetName = "NewDataSet"
		'
		'frmBoletos
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.ClientSize = New System.Drawing.Size(640, 345)
		Me.Controls.Add(Me.dataGridView1)
		Me.Controls.Add(Me.btnEnvio)
		Me.Controls.Add(Me.btnCerrar)
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
		Me.KeyPreview = true
		Me.MinimizeBox = false
		Me.MinimumSize = New System.Drawing.Size(600, 345)
		Me.Name = "frmBoletos"
		Me.ShowIcon = false
		Me.ShowInTaskbar = false
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Consulta de Boletos"
		AddHandler Load, AddressOf Me.FrmBoletosLoad
		AddHandler KeyUp, AddressOf Me.FrmBoletosKeyUp
		Me.cmsMenu.ResumeLayout(false)
		CType(Me.dataGridView1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.dsEnvios,System.ComponentModel.ISupportInitialize).EndInit
		Me.ResumeLayout(false)
	End Sub
	Private dsEnvios As System.Data.DataSet
	Private dataGridView1 As System.Windows.Forms.DataGridView
	Private btnEnvio As System.Windows.Forms.Button
	Private toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
	Private toolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
	Private toolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
	Private cmsMenu As System.Windows.Forms.ContextMenuStrip
	Private btnCerrar As System.Windows.Forms.Button
	'
	Private conn    As New OracleConnection()
	'
	Private connSS  As New SqlClient.SqlConnection
	Private connOLE As New OleDb.OleDbConnection
	'
	Private connGrd As New OleDb.OleDbConnection 'SqlClient.SqlConnection
	Private xjmt    As New OracleConnection()
	Private vExiste As Boolean
End Class
