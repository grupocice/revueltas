﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 07/02/2017
' Hora: 08:24 a. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports System.Data.OleDb
Imports System.Data
Imports Oracle.DataAccess.Client
Imports System.Configuration





Partial Public Class frmModificacion






    Public Sub New()

        Me.InitializeComponent()
        cmbProducto.Enabled = False
        cmbEmpresa.Enabled = False
        cmbTransportista.Enabled = False
        btLimpiar.Enabled = False

    End Sub

    Public Function obtenerMercancias() As Integer

        Dim bandera As Integer = 0
        Dim tipoTransportista As String = ""
        Try


            ConectarseJMT()
            Dim query As String = " SELECT pro_nombre, PRO_ID FROM PRODUCTOS_REV " +
                                  " WHERE pro_id NOT IN ('01') " +
                                  " AND pro_nombre <> 'PRUEBA' " +
                                  " ORDER BY NLSSORT(pro_id,'NLS_SORT=SPANISH') ASC "



            Dim Cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            Cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                cmbProducto.Items.Add(reader.GetValue(0).ToString())
                bandera += 1
            End While

            Dim query2 As String = " SELECT DISTINCT RR.PRO_NOMBRE  FROM PRODUCTOS_REV RR " +
                                   " WHERE 1 = 1" +
                                   " AND RR.PRO_ID  = (SELECT DISTINCT P.PRO_ID FROM PESADAS P,  PRODUCTOS_REV PR " +
                                   " WHERE 1 = 1 " +
                                   " AND P.PRO_ID = PR.PRO_ID " +
                                   " AND P.PES_ID = " + txtBoleto.Text + " ) "

            Dim Cadena2 As String
            custDS = New DataSet
            Dim selectCMD2 As OracleCommand = New OracleCommand(query2, xjmt)
            Dim custDA2 As OracleDataAdapter = New OracleDataAdapter
            Cadena2 = xjmt.ConnectionString
            Dim reader2 As OracleDataReader = selectCMD2.ExecuteReader()

            While reader2.Read()
                cmbProducto.Items.Insert(0, reader2.GetValue(0).ToString())
                cmbProducto.SelectedIndex = 0
            End While



        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerMercancias, error " + ex.ToString())

        End Try

        xjmt.Close()


        Return bandera


    End Function

    Public Function obtenerEmpresas() As Integer

        Dim bandera As Integer = 0
        Try


            ConectarseJMT()
            Dim query As String = " SELECT DISTINCT EMP_NOMBRE FROM EMPRESAS_REV " +
                                  " ORDER BY NLSSORT(EMP_NOMBRE, 'NLS_SORT = SPANISH') ASC "



            Dim Cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            Cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                cmbEmpresa.Items.Add(reader.GetValue(0).ToString())
                bandera += 1
            End While


            '  cmbProducto.Items.Insert(0, "ARROZ")
            ' cmbProducto.SelectedIndex = 0

            Dim query2 As String = "SELECT DISTINCT EMP_NOMBRE FROM EMPRESAS_REV RR" +
                                   " WHERE 1 = 1" +
                                   " AND RR.EMP_ID = (SELECT  DISTINCT P.emp_id FROM PESADAS P, EMPRESAS_REV E" +
                                   " WHERE 1 = 1 " +
                                   " AND P.EMP_ID = E.EMP_ID  " +
                                   " AND P.pes_id = " + txtBoleto.Text + " ) "

            Dim Cadena2 As String
            custDS = New DataSet
            Dim selectCMD2 As OracleCommand = New OracleCommand(query2, xjmt)
            Dim custDA2 As OracleDataAdapter = New OracleDataAdapter
            Cadena2 = xjmt.ConnectionString
            Dim reader2 As OracleDataReader = selectCMD2.ExecuteReader()

            While reader2.Read()
                cmbEmpresa.Items.Insert(0, reader2.GetValue(0).ToString())
                cmbEmpresa.SelectedIndex = 0
            End While

        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerEmpresa, error " + ex.ToString())

        End Try

        xjmt.Close()


        Return bandera


    End Function

    Public Function obtenerAdicional(eco As String, ban As String) As String
        Dim valor As String = ""


        Try
            ConectarseJMT()
            Dim query As String = " Select Get_Adicional(" + eco + "," + Chr(39) + ban + Chr(39) + ") FROM DUAL  "


            Dim cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                valor = reader.GetValue(0).ToString()

            End While

            If valor = vbNullString Then
                valor = " "
            End If

        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerAdicional de la clase Adicionales, error: " + ex.Message)
        End Try
        xjmt.Close()

        Return valor

    End Function


    Public Function obtenerTranspostistas() As Integer

        Dim bandera As Integer = 0
        Try


            ConectarseJMT()
            Dim query As String = " SELECT DISTINCT tra_nombre FROM TRANSPORTISTAS_REV " +
                                  " ORDER BY tra_nombre ASC "



            Dim Cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            Cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                cmbTransportista.Items.Add(reader.GetValue(0).ToString())
                bandera += 1
            End While

            Dim query2 As String = "SELECT DISTINCT  RR.TRA_NOMBRE FROM TRANSPORTISTAS_REV RR" +
                                  " WHERE 1 = 1" +
                                  " AND RR.TRA_ID = (SELECT DISTINCT P.TRA_ID FROM PESADAS P, TRANSPORTISTAS_REV PR" +
                                  " WHERE 1 = 1 " +
                                  " AND P.TRA_ID = PR.TRA_ID  " +
                                  " AND P.PES_ID = " + txtBoleto.Text + " ) "

            Dim Cadena2 As String
            custDS = New DataSet
            Dim selectCMD2 As OracleCommand = New OracleCommand(query2, xjmt)
            Dim custDA2 As OracleDataAdapter = New OracleDataAdapter
            Cadena2 = xjmt.ConnectionString
            Dim reader2 As OracleDataReader = selectCMD2.ExecuteReader()

            While reader2.Read()
                cmbTransportista.Items.Insert(0, reader2.GetValue(0).ToString())
                cmbTransportista.SelectedIndex = 0
            End While




        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerTranspostistas, error " + ex.ToString())

        End Try

        xjmt.Close()


        Return bandera


    End Function

    '
    Public Sub ConectarseJMT()
        Dim cadena As String
        cadena = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString

        Dim oradb As String
        oradb = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionJMT").ConnectionString
        oradb = String.Format(oradb, My.Settings.IP, My.Settings.Usuario, My.Settings.Contrasena)
        xjmt.ConnectionString = oradb
        '
    End Sub
    '
    Sub ActualizaJMT()
        '
        Dim vQuery As String
        Dim vCamVagon As String
        Dim vPlacas As String
        Dim vTag As String
        Dim pro_id As String
        Dim emp_id As String
        Dim trans_id As String
        Dim transportista As String
        Dim pedimento As String

        pro_id = obtenerIdProducto()
        emp_id = obtenerIdEmpresa()

        ' If cmbTransportista.Text.Trim = "" Or cmbTransportista.Text = "S/T" Then
        'transportista = "S/T"
        'Else
        transportista = cmbTransportista.Text.ToString()
        trans_id = obtenerIdTransportista()
        'End If


        ' If transportista = "S/T" Then
        'trans_id = "S/ID"
        'Else

        'End If

        If cmbTipoTransporte.Text.ToUpper.Trim = "TRACTOCAMION" Then
            vCamVagon = Chr(39) + "C" + Chr(39)
        Else
            vCamVagon = Chr(39) + "V" + Chr(39)
        End If
        '
        vPlacas = Chr(39) + txtPlaca.Text + Chr(39)
        vTag = Chr(39) + txtTagID.Text + Chr(39)

        '  If txtTagID.Text = "" Then
        'vTag = Chr(39) + "S/T" + Chr(39)
        'End If

        If txtTagID.Text = "" Then
            vTag = "null"
        End If

        If txtPedi.Text = "" Then
            pedimento = ""
        Else
            pedimento = txtPedi.Text.ToUpper.Trim
        End If

        Try
            vQuery = "  UPDATE PESADAS " +
                     "  SET PES_CAMVAGON = " + vCamVagon +
                     ", PES_PLACAS = " + vPlacas +
                     ", TAG_ID = " + vTag +
                     ", PRO_NOMBRE =  " + Chr(39) + cmbProducto.Text.ToString() + Chr(39) +
                     ", PRO_ID =  " + Chr(39) + pro_id.ToString() + Chr(39) +
                     ", EMP_NOMBRE = " + Chr(39) + cmbEmpresa.Text.ToString() + Chr(39) +
                     ", EMP_ID = " + Chr(39) + emp_id.ToString() + Chr(39) +
                     ", PES_BRUTO = " + txtBruto.Text.ToString() +
                     ", PES_NETO = " + txtNeto.Text.ToString() +
                     ", PES_TARA = " + txtTara.Text.ToString() +
                     ", TRA_NOMBRE = " + Chr(39) + transportista + Chr(39) +
                     ", TRA_ID = " + Chr(39) + trans_id + Chr(39) +
                     ", PES_ADICIONAL = " + Chr(39) + pedimento + Chr(39) +
                     " WHERE PES_ID = " + txtBoletoBack.Text


            xjmt.Open()
            Dim ddataAdapter = New OracleDataAdapter(vQuery, xjmt)
            ddataAdapter.SelectCommand.ExecuteNonQuery()
            '

            MessageBox.Show("Ticket: " + txtBoletoBack.Text + " Actualizado correctamente")

        Catch ex As Exception
            MessageBox.Show("Error en el metodo Actualizar en en query que actualiza  " & vbCrLf & ex.Message)
        Finally
            xjmt.Close()

        End Try
    End Sub

    Sub ActualizaAdicionales()

        Dim respuesta As String = ""
        Dim tipoAdicional As Integer

        Dim adicionales(4) As String
        adicionales = New String() {"ECONOMICO", "MARCAS", "REMESA", "BUQUE"}

        For ctr As Integer = 0 To 3
            Dim bandera As String = adicionales(ctr)
            Select Case bandera
                Case "ECONOMICO"
                    tipoAdicional = 4
                    respuesta = respuesta + operacionesAdicionales(4)
                Case "MARCAS"
                    tipoAdicional = 10
                    respuesta = respuesta + operacionesAdicionales(10)
                Case "REMESA"
                    tipoAdicional = 5
                    respuesta = respuesta + operacionesAdicionales(5)
                Case Else
                    tipoAdicional = 6
                    respuesta = respuesta + operacionesAdicionales(6)
            End Select
        Next
        MessageBox.Show(respuesta)
        Process.Start("iexplore", "http://www.grupocice.com/reports/rwservlet?vgmpesada&p_ticket=" + txtBoleto.Text.ToString() + "&p_numpesada=2")
    End Sub

    Public Function operacionesAdicionales(tipoAdicional As Integer) As String
        Dim respuesta As String = ""
        Dim tipo As String = ""
        Dim query As String

        Try
            ConectarseJMT()
            
            Select Case tipoAdicional
                Case 4
                    tipo = " ECONOMICO "
                    query = " BEGIN " +
                            " UPDATE PESVALADI " +
                            " SET PVA_VALOR = " + Chr(39) + txtEconomico.Text.ToString() + Chr(39) +
                            " WHERE PES_ID =  " + txtBoleto.Text.ToString() +
                            " AND CAR_ID = 4; " +
                             " COMMIT; " +
                            " IF SQL%ROWCOUNT = 0 THEN  " +
                            " INSERT INTO PESVALADI(PES_ID, CAR_ID, PVA_VALOR) " +
                            " VALUES ( " + txtBoleto.Text.ToString() + ", 4 ," + Chr(39) + txtEconomico.Text.ToString() + Chr(39) + " ); " +
                            " END IF; " +
                            " COMMIT; " +
                            " END; "
                    'MessageBox.Show("query " + query)
                    xjmt.Open()
                    Dim ddataAdapter = New OracleDataAdapter(query, xjmt)
                    ddataAdapter.SelectCommand.ExecuteNonQuery()
                    respuesta = " ok ECONOMICO"

                Case 5
                    tipo = " REMESA "
                    query = " BEGIN " +
                            " UPDATE PESVALADI " +
                            " SET PVA_VALOR = " + Chr(39) + txtRemesa.Text.ToString() + Chr(39) +
                            " WHERE PES_ID =  " + txtBoleto.Text.ToString() +
                            " AND CAR_ID = 5 ; " +
                             " COMMIT; " +
                            " IF SQL%ROWCOUNT = 0 THEN  " +
                            " INSERT INTO PESVALADI(PES_ID, CAR_ID, PVA_VALOR) " +
                            " VALUES ( " + txtBoleto.Text.ToString() + ", 5 ," + Chr(39) + txtRemesa.Text.ToString() + Chr(39) + " ); " +
                            " END IF; " +
                            " COMMIT; " +
                            " END; "
                    xjmt.Open()
                    Dim ddataAdapter = New OracleDataAdapter(query, xjmt)
                    ddataAdapter.SelectCommand.ExecuteNonQuery()
                    respuesta = " ok REMESA"
                Case 6
                    tipo = " BUQUE "
                    query = " BEGIN " +
                            " UPDATE PESVALADI " +
                            " SET PVA_VALOR = " + Chr(39) + txtBuque.Text.ToString() + Chr(39) +
                            " WHERE PES_ID =  " + txtBoleto.Text.ToString() +
                            " AND CAR_ID = 6 ; " +
                             " COMMIT; " +
                            " IF SQL%ROWCOUNT = 0 THEN  " +
                            " INSERT INTO PESVALADI(PES_ID, CAR_ID, PVA_VALOR) " +
                            " VALUES ( " + txtBoleto.Text.ToString() + ", 6 ," + Chr(39) + txtBuque.Text.ToString() + Chr(39) + " ); " +
                            " END IF; " +
                            " COMMIT; " +
                            " END; "
                    xjmt.Open()
                    Dim ddataAdapter = New OracleDataAdapter(query, xjmt)
                    ddataAdapter.SelectCommand.ExecuteNonQuery()
                    respuesta = " ok BUQUE"
                Case Else
                    tipo = " MARCAS "
                    query = " BEGIN " +
                            " UPDATE PESVALADI " +
                            " SET PVA_VALOR = " + Chr(39) + txtMarcas.Text.ToString() + Chr(39) +
                            " WHERE PES_ID =  " + txtBoleto.Text.ToString() +
                            " AND CAR_ID = 10 ; " +
                             " COMMIT; " +
                            " IF SQL%ROWCOUNT = 0 THEN  " +
                            " INSERT INTO PESVALADI(PES_ID, CAR_ID, PVA_VALOR) " +
                            " VALUES ( " + txtBoleto.Text.ToString() + ", 10 ," + Chr(39) + txtMarcas.Text.ToString() + Chr(39) + " ); " +
                            " END IF; " +
                            " COMMIT; " +
                            " END; "
                    xjmt.Open()
                    Dim ddataAdapter = New OracleDataAdapter(query, xjmt)
                    ddataAdapter.SelectCommand.ExecuteNonQuery()
                    respuesta = " ok MARCAS"
            End Select


        Catch ex As Exception

            If tipoAdicional = 4 Then
                MessageBox.Show("No se puedo actualizar  el ECONOMICO debido a " + ex.Message)
            ElseIf tipoAdicional = 5 Then
                MessageBox.Show("No se puedo actualizar  la REMESA debido a " + ex.Message)
            ElseIf tipoAdicional = 6 Then
                MessageBox.Show("No se puedo actualizar  el BUQUE debido a " + ex.Message)
            Else
                MessageBox.Show("No se puedo actualizar  las MARCAS debido a " + ex.Message)
            End If
        Finally
            xjmt.Close()
        End Try

        Return respuesta
    End Function
    '
    Public Function obtenerIdProducto() As String
        Dim idProducto As String = ""
        Try
            ConectarseJMT()
            Dim query As String = " SELECT DISTINCT PRO_ID FROM PRODUCTOS_REV " +
                                  " WHERE PRO_NOMBRE LIKE '%" + cmbProducto.Text.ToString() + "%' "
            Dim Cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            Cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                idProducto = reader.GetValue(0).ToString()
            End While

        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerIdProducto, error " + ex.Message.ToString())
        End Try
        xjmt.Close()
        Return idProducto
    End Function

    Public Function obtenerIdEmpresa() As String
        Dim idEmpresa As String = ""
        Try
            ConectarseJMT()
            Dim query As String = " SELECT DISTINCT EMP_ID FROM EMPRESAS_REV " +
                                  " WHERE EMP_NOMBRE LIKE '%" + cmbEmpresa.Text.ToString() + "%' " +
                                  " AND EMP_ID <> '1' "
            Dim Cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            Cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                idEmpresa = reader.GetValue(0).ToString()
            End While
        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerIdEmpresa, error " + ex.Message.ToString())
        End Try
        xjmt.Close()
        Return idEmpresa
    End Function

    Public Function obtenerIdTransportista() As String
        Dim id As String = ""
        Try
            ConectarseJMT()
            Dim query As String = " SELECT DISTINCT tra_id FROM TRANSPORTISTAS_REV " +
                                  " WHERE tra_nombre LIKE '%" + cmbTransportista.Text.ToString() + "%' "

            Dim Cadena As String
            custDS = New DataSet
            Dim selectCMD As OracleCommand = New OracleCommand(query, xjmt)
            Dim custDA As OracleDataAdapter = New OracleDataAdapter
            Cadena = xjmt.ConnectionString
            xjmt.Open()

            Dim reader As OracleDataReader = selectCMD.ExecuteReader()

            While reader.Read()
                id = reader.GetValue(0).ToString()
            End While
        Catch ex As Exception
            MessageBox.Show("Error en el metodo obtenerIdTransportista, error " + ex.Message.ToString())
        End Try
        xjmt.Close()
        Return id
    End Function


    Sub GetDataJMT(ByVal selectCommand As String)
        '
        Dim Cadena As String
        custDS = New DataSet
        Dim selectCMD As OracleCommand = New OracleCommand(selectCommand, xjmt)
        Dim custDA As OracleDataAdapter = New OracleDataAdapter
        '
        Cadena = xjmt.ConnectionString
        selectCMD.CommandTimeout = 30
        custDA.SelectCommand = selectCMD
        custDA.Fill(custDS, "Datos")
        '
    End Sub
    '
    Sub BtnConsultarClick(sender As Object, e As EventArgs) Handles btnConsultar.Click



        If txtBoleto.Text <> "" Then

            Dim vQuery As String
            Dim ddataRow As DataRow

            ' PARA LOS ADICIONALES
            Dim economico As String
            Dim marcas As String
            Dim conocimiento As String
            Dim remesa As String
            Dim buque As String
            Dim tipoAdicional As String


            ' Prueba con un dato x a Carga
            If (xjmt.State = System.Data.ConnectionState.Closed) Or (xjmt.State = System.Data.ConnectionState.Broken) Then
                ConectarseJMT()
            End If
            '
            vQuery = "SELECT Pes_ID, Pes_Placas, Tag_ID, Pes_CamVagon, Pes_PedID, Ord_ID, Pes_Bruto, Pes_Tara, Pes_Neto, Usb_ID, Usb_Nombre, Pro_ID, Pro_Nombre, Emp_ID, Emp_Nombre, Emp_Categoria, Tra_ID, Tra_Nombre, PES_ADICIONAL " & _
                     " from Pesadas " & _
                     " where Pes_ID = " + txtBoleto.Text
            GetDataJMT(vQuery)




            '
            Dim nCantidad As Integer = custDS.Tables(0).Rows.Count

            If nCantidad > 0 Then


                btnConsultar.Enabled = False
                btLimpiar.Enabled = True
                txtBoleto.Enabled = False

                ddataRow = custDS.Tables(0).Rows(0)

                If ddataRow!Pes_CamVagon.ToString = "C" Then
                    cmbTipoTransporte.Text = "TractoCamion"
                Else
                    cmbTipoTransporte.Text = "Ferroviario"
                End If

                Dim valor As Integer = obtenerMercancias()

                If valor > 0 Then
                    cmbProducto.Enabled = True
                Else
                    MessageBox.Show(" No se puede obtener el catalago de tipo de mercancias ")
                End If

                Dim valor2 As Integer = obtenerEmpresas()
                If valor2 > 0 Then
                    cmbEmpresa.Enabled = True
                Else
                    MessageBox.Show(" No se puede obtener el catalago de tipo de empresas ")
                End If

                Dim valor3 As Integer = obtenerTranspostistas()
                If valor3 > 0 Then
                    cmbTransportista.Enabled = True
                Else
                    MessageBox.Show(" No se puede obtener el catalago de tipo de Transpostistas ")
                End If

                Dim adicionales(4) As String
                adicionales = New String() {"ECONOMICO", "MARCAS", "REMESA", "BUQUE"}

                For ctr As Integer = 0 To 3
                    ' MessageBox.Show(adicionales(ctr))
                    Dim bandera As String = adicionales(ctr)
                    Select Case bandera
                        Case "ECONOMICO"
                            tipoAdicional = "ECONOMICO"
                            economico = obtenerAdicional(txtBoleto.Text.ToString(), tipoAdicional)
                            txtEconomico.Text = economico
                        Case "MARCAS"
                            tipoAdicional = "MARCAS"
                            marcas = obtenerAdicional(txtBoleto.Text.ToString(), tipoAdicional)
                            txtMarcas.Text = marcas
                        Case "REMESA"
                            tipoAdicional = "REMESA"
                            remesa = obtenerAdicional(txtBoleto.Text.ToString(), tipoAdicional)
                            txtRemesa.Text = remesa
                        Case Else
                            tipoAdicional = "BUQUE"
                            buque = obtenerAdicional(txtBoleto.Text.ToString(), tipoAdicional)
                            txtBuque.Text = buque
                    End Select


                Next

                txtBoletoBack.Text = txtBoleto.Text
                txtTagID.Text = ddataRow!Tag_ID.ToString
                txtPlaca.Text = ddataRow!Pes_Placas.ToString
                'txtChofer.Text = ddataRow!Pes_Chofer.ToString
                txtEmpresa.Text = ddataRow!Emp_Nombre.ToString
                txtProducto.Text = ddataRow!Pro_Nombre.ToString
                txtBruto.Text = ddataRow!Pes_Bruto.ToString
                txtTara.Text = ddataRow!Pes_Tara.ToString
                txtNeto.Text = ddataRow!Pes_Neto.ToString
                txtPedi.Text = ddataRow!PES_ADICIONAL.ToString
                panel1.Enabled = True

                '
            Else
                MessageBox.Show("No Existe el registro del Boleto de Pesaje")
                cmbTipoTransporte.Text = vbNullString
                txtBoletoBack.Text = vbNullString
                txtTagID.Text = vbNullString
                txtPlaca.Text = vbNullString
                'txtChofer.Text = ddataRow!Pes_Chofer.ToString
                txtEmpresa.Text = vbNullString
                txtProducto.Text = vbNullString
                txtBruto.Text = vbNullString
                txtTara.Text = vbNullString
                txtNeto.Text = vbNullString
                panel1.Enabled = False
            End If

        End If
        '
        txtBoleto.Focus()
        txtBoleto.SelectAll()
        '		
    End Sub

    Sub FrmModificacionShown(sender As Object, e As EventArgs) Handles MyBase.Shown
        txtBoleto.Focus()
    End Sub

    Sub FrmModificacionActivated(sender As Object, e As EventArgs) Handles MyBase.Activated
        txtBoleto.Focus()
    End Sub

    Sub TxtBoletoKeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBoleto.KeyPress
        Dim tmp As System.Windows.Forms.KeyPressEventArgs = e
        '
        If tmp.KeyChar = ChrW(Keys.Enter) Then
            Call BtnConsultarClick(sender, e)
        End If

        If (Char.IsDigit(e.KeyChar)) Then

            e.Handled = False

        ElseIf (Char.IsControl(e.KeyChar)) Then

            e.Handled = False

        Else

            e.Handled = True

        End If
    End Sub

    Sub Button1Click(sender As Object, e As EventArgs) Handles button1.Click
        'Application.Exit
        conn.Close()
        connSS.Close()
        connOLE.Close()
        connGrd.Close()
        xjmt.Close()
        '
        Me.Close()
    End Sub

    Sub BtnActualizaClick(sender As Object, e As EventArgs) Handles btnActualiza.Click



        If txtBoletoBack.Text.Trim = "" Then
            MessageBox.Show("No se ha capturado ticket")


        ElseIf cmbTipoTransporte.Text = "" Then
            MessageBox.Show("Falta capturar el tipo de Transporte")


        ElseIf cmbTipoTransporte.Text.ToUpper <> "TRACTOCAMION" And cmbTipoTransporte.Text.ToUpper <> "FERROVIARIO" Then
            MessageBox.Show("No es el tipo de Transporte")
        ElseIf cmbProducto.Text.Trim = "" Then
            MessageBox.Show("Debe elegir un tipo de mercancia")
        ElseIf cmbEmpresa.Text.Trim = "" Then
            MessageBox.Show("Debe elegir un tipo de mercancia")
        ElseIf txtBruto.Text.Trim = "" Then
            MessageBox.Show("Debe ingresar un peso bruto")
        ElseIf txtNeto.Text.Trim = "" Then
            MessageBox.Show("Debe ingresar un peso neto")
        ElseIf txtTara.Text.Trim = "" Then
            MessageBox.Show("Debe ingresar el peso de la tara")
        Else
            ActualizaJMT()
            ActualizaAdicionales()
        End If
        '
        txtBoleto.Focus()
    End Sub

    Sub FrmModificacionLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        panel1.Enabled = False
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btLimpiar_Click(sender As Object, e As EventArgs) Handles btLimpiar.Click
        panel1.Enabled = False
        btnConsultar.Enabled = True
        txtBoleto.Enabled = True
        btLimpiar.Enabled = False
        cmbTipoTransporte.Text = vbNullString
        txtBoletoBack.Text = vbNullString
        txtTagID.Text = vbNullString
        txtPlaca.Text = vbNullString
        txtEmpresa.Text = vbNullString
        txtProducto.Text = vbNullString
        txtBruto.Text = vbNullString
        txtTara.Text = vbNullString
        txtNeto.Text = vbNullString
        txtEconomico.Text = vbNullString
        txtMarcas.Text = vbNullString
        txtPedi.Text = vbNullString
        txtRemesa.Text = vbNullString
        txtBuque.Text = vbNullString
        txtBoleto.Text = ""
        txtBoleto.Focus()
        cmbTransportista.Enabled = True
        cmbEmpresa.Items.Clear()
        cmbProducto.Items.Clear()
        cmbTipoTransporte.Items.Clear()





    End Sub

    Private Sub txtBruto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBruto.KeyPress
        If (Char.IsDigit(e.KeyChar)) Then

            e.Handled = False

        ElseIf (Char.IsControl(e.KeyChar)) Then

            e.Handled = False

        Else

            e.Handled = True

        End If

    End Sub

    Private Sub txtTara_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTara.KeyPress
        If (Char.IsDigit(e.KeyChar)) Then

            e.Handled = False

        ElseIf (Char.IsControl(e.KeyChar)) Then

            e.Handled = False

        Else

            e.Handled = True

        End If
    End Sub

    Private Sub txtNeto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNeto.KeyPress
        If (Char.IsDigit(e.KeyChar)) Then

            e.Handled = False

        ElseIf (Char.IsControl(e.KeyChar)) Then

            e.Handled = False

        Else

            e.Handled = True

        End If
    End Sub
End Class
