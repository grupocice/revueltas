﻿'
' Creado por SharpDevelop.
' Usuario: ProyectosTI2
' Fecha: 07/02/2017
' Hora: 08:24 a. m.
' 
' Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
'
Imports Oracle.DataAccess.Client
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Net
Imports System.Data.OleDb

Partial Class frmModificacion
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmModificacion))
        Me.panel1 = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtBuque = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtRemesa = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtPedi = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMarcas = New System.Windows.Forms.TextBox()
        Me.Economico = New System.Windows.Forms.Label()
        Me.txtEconomico = New System.Windows.Forms.TextBox()
        Me.cmbTransportista = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbEmpresa = New System.Windows.Forms.ComboBox()
        Me.cmbProducto = New System.Windows.Forms.ComboBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.cmbTipoTransporte = New System.Windows.Forms.ComboBox()
        Me.txtNeto = New System.Windows.Forms.TextBox()
        Me.label9 = New System.Windows.Forms.Label()
        Me.label8 = New System.Windows.Forms.Label()
        Me.label7 = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.txtTagID = New System.Windows.Forms.TextBox()
        Me.txtTara = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.txtBruto = New System.Windows.Forms.TextBox()
        Me.txtPlaca = New System.Windows.Forms.TextBox()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtBoleto = New System.Windows.Forms.TextBox()
        Me.btnActualiza = New System.Windows.Forms.Button()
        Me.button1 = New System.Windows.Forms.Button()
        Me.txtBoletoBack = New System.Windows.Forms.TextBox()
        Me.btLimpiar = New System.Windows.Forms.Button()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.txtEmpresa = New System.Windows.Forms.TextBox()
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel1
        '
        Me.panel1.BackColor = System.Drawing.Color.White
        Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel1.Controls.Add(Me.Label14)
        Me.panel1.Controls.Add(Me.txtBuque)
        Me.panel1.Controls.Add(Me.Label13)
        Me.panel1.Controls.Add(Me.txtRemesa)
        Me.panel1.Controls.Add(Me.Label12)
        Me.panel1.Controls.Add(Me.txtPedi)
        Me.panel1.Controls.Add(Me.Label11)
        Me.panel1.Controls.Add(Me.txtMarcas)
        Me.panel1.Controls.Add(Me.Economico)
        Me.panel1.Controls.Add(Me.txtEconomico)
        Me.panel1.Controls.Add(Me.cmbTransportista)
        Me.panel1.Controls.Add(Me.Label4)
        Me.panel1.Controls.Add(Me.cmbEmpresa)
        Me.panel1.Controls.Add(Me.cmbProducto)
        Me.panel1.Controls.Add(Me.label1)
        Me.panel1.Controls.Add(Me.cmbTipoTransporte)
        Me.panel1.Controls.Add(Me.txtNeto)
        Me.panel1.Controls.Add(Me.label9)
        Me.panel1.Controls.Add(Me.label8)
        Me.panel1.Controls.Add(Me.label7)
        Me.panel1.Controls.Add(Me.label6)
        Me.panel1.Controls.Add(Me.label5)
        Me.panel1.Controls.Add(Me.label10)
        Me.panel1.Controls.Add(Me.txtTagID)
        Me.panel1.Controls.Add(Me.txtTara)
        Me.panel1.Controls.Add(Me.label3)
        Me.panel1.Controls.Add(Me.txtBruto)
        Me.panel1.Controls.Add(Me.txtPlaca)
        Me.panel1.Location = New System.Drawing.Point(13, 69)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(893, 306)
        Me.panel1.TabIndex = 41
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(14, 238)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 16)
        Me.Label14.TabIndex = 61
        Me.Label14.Text = "Buque"
        '
        'txtBuque
        '
        Me.txtBuque.Location = New System.Drawing.Point(17, 257)
        Me.txtBuque.MaxLength = 40
        Me.txtBuque.Name = "txtBuque"
        Me.txtBuque.Size = New System.Drawing.Size(148, 20)
        Me.txtBuque.TabIndex = 60
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(387, 159)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 59
        Me.Label13.Text = "Remesa"
        '
        'txtRemesa
        '
        Me.txtRemesa.Location = New System.Drawing.Point(390, 178)
        Me.txtRemesa.MaxLength = 40
        Me.txtRemesa.Name = "txtRemesa"
        Me.txtRemesa.Size = New System.Drawing.Size(148, 20)
        Me.txtRemesa.TabIndex = 58
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(208, 159)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(91, 16)
        Me.Label12.TabIndex = 57
        Me.Label12.Text = "Pedimento"
        '
        'txtPedi
        '
        Me.txtPedi.Location = New System.Drawing.Point(211, 178)
        Me.txtPedi.MaxLength = 20
        Me.txtPedi.Name = "txtPedi"
        Me.txtPedi.Size = New System.Drawing.Size(148, 20)
        Me.txtPedi.TabIndex = 56
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(14, 159)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(91, 16)
        Me.Label11.TabIndex = 55
        Me.Label11.Text = "Marcas"
        '
        'txtMarcas
        '
        Me.txtMarcas.Location = New System.Drawing.Point(17, 178)
        Me.txtMarcas.MaxLength = 40
        Me.txtMarcas.Name = "txtMarcas"
        Me.txtMarcas.Size = New System.Drawing.Size(148, 20)
        Me.txtMarcas.TabIndex = 54
        '
        'Economico
        '
        Me.Economico.Location = New System.Drawing.Point(583, 159)
        Me.Economico.Name = "Economico"
        Me.Economico.Size = New System.Drawing.Size(91, 16)
        Me.Economico.TabIndex = 53
        Me.Economico.Text = "Economico"
        '
        'txtEconomico
        '
        Me.txtEconomico.Location = New System.Drawing.Point(586, 178)
        Me.txtEconomico.MaxLength = 40
        Me.txtEconomico.Name = "txtEconomico"
        Me.txtEconomico.Size = New System.Drawing.Size(148, 20)
        Me.txtEconomico.TabIndex = 52
        '
        'cmbTransportista
        '
        Me.cmbTransportista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTransportista.FormattingEnabled = True
        Me.cmbTransportista.Items.AddRange(New Object() {"S/T"})
        Me.cmbTransportista.Location = New System.Drawing.Point(612, 109)
        Me.cmbTransportista.Name = "cmbTransportista"
        Me.cmbTransportista.Size = New System.Drawing.Size(246, 21)
        Me.cmbTransportista.TabIndex = 51
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(609, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(151, 21)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "Transportista"
        '
        'cmbEmpresa
        '
        Me.cmbEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEmpresa.FormattingEnabled = True
        Me.cmbEmpresa.Location = New System.Drawing.Point(17, 110)
        Me.cmbEmpresa.Name = "cmbEmpresa"
        Me.cmbEmpresa.Size = New System.Drawing.Size(246, 21)
        Me.cmbEmpresa.TabIndex = 49
        '
        'cmbProducto
        '
        Me.cmbProducto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProducto.FormattingEnabled = True
        Me.cmbProducto.Location = New System.Drawing.Point(317, 110)
        Me.cmbProducto.Name = "cmbProducto"
        Me.cmbProducto.Size = New System.Drawing.Size(246, 21)
        Me.cmbProducto.TabIndex = 48
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(317, 23)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(91, 16)
        Me.label1.TabIndex = 41
        Me.label1.Text = "Tag ID"
        '
        'cmbTipoTransporte
        '
        Me.cmbTipoTransporte.FormattingEnabled = True
        Me.cmbTipoTransporte.Items.AddRange(New Object() {"TractoCamion", "Ferroviario"})
        Me.cmbTipoTransporte.Location = New System.Drawing.Point(17, 52)
        Me.cmbTipoTransporte.Name = "cmbTipoTransporte"
        Me.cmbTipoTransporte.Size = New System.Drawing.Size(246, 21)
        Me.cmbTipoTransporte.TabIndex = 40
        '
        'txtNeto
        '
        Me.txtNeto.Location = New System.Drawing.Point(492, 257)
        Me.txtNeto.MaxLength = 8
        Me.txtNeto.Name = "txtNeto"
        Me.txtNeto.Size = New System.Drawing.Size(100, 20)
        Me.txtNeto.TabIndex = 8
        '
        'label9
        '
        Me.label9.Location = New System.Drawing.Point(492, 237)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(57, 14)
        Me.label9.TabIndex = 34
        Me.label9.Text = "Peso Neto"
        '
        'label8
        '
        Me.label8.Location = New System.Drawing.Point(347, 240)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(65, 14)
        Me.label8.TabIndex = 33
        Me.label8.Text = "Tara"
        '
        'label7
        '
        Me.label7.Location = New System.Drawing.Point(200, 240)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(63, 14)
        Me.label7.TabIndex = 32
        Me.label7.Text = "Peso Bruto"
        '
        'label6
        '
        Me.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.label6.Location = New System.Drawing.Point(317, 92)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(50, 15)
        Me.label6.TabIndex = 31
        Me.label6.Text = "Producto"
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(14, 92)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(50, 15)
        Me.label5.TabIndex = 30
        Me.label5.Text = "Cliente"
        '
        'label10
        '
        Me.label10.Location = New System.Drawing.Point(14, 23)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(99, 20)
        Me.label10.TabIndex = 39
        Me.label10.Text = "Tipo Transporte"
        '
        'txtTagID
        '
        Me.txtTagID.Location = New System.Drawing.Point(320, 55)
        Me.txtTagID.MaxLength = 20
        Me.txtTagID.Name = "txtTagID"
        Me.txtTagID.Size = New System.Drawing.Size(148, 20)
        Me.txtTagID.TabIndex = 1
        '
        'txtTara
        '
        Me.txtTara.Location = New System.Drawing.Point(346, 257)
        Me.txtTara.MaxLength = 8
        Me.txtTara.Name = "txtTara"
        Me.txtTara.Size = New System.Drawing.Size(100, 20)
        Me.txtTara.TabIndex = 7
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(543, 23)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(50, 20)
        Me.label3.TabIndex = 28
        Me.label3.Text = "Placas"
        '
        'txtBruto
        '
        Me.txtBruto.Location = New System.Drawing.Point(200, 257)
        Me.txtBruto.MaxLength = 8
        Me.txtBruto.Name = "txtBruto"
        Me.txtBruto.Size = New System.Drawing.Size(100, 20)
        Me.txtBruto.TabIndex = 6
        '
        'txtPlaca
        '
        Me.txtPlaca.Location = New System.Drawing.Point(546, 55)
        Me.txtPlaca.Name = "txtPlaca"
        Me.txtPlaca.ReadOnly = True
        Me.txtPlaca.Size = New System.Drawing.Size(148, 20)
        Me.txtPlaca.TabIndex = 2
        '
        'btnConsultar
        '
        Me.btnConsultar.BackColor = System.Drawing.Color.Transparent
        Me.btnConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnConsultar.Image = CType(resources.GetObject("btnConsultar.Image"), System.Drawing.Image)
        Me.btnConsultar.Location = New System.Drawing.Point(128, 12)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(110, 39)
        Me.btnConsultar.TabIndex = 43
        Me.btnConsultar.TabStop = False
        Me.btnConsultar.Text = "Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = False
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(12, 12)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(93, 16)
        Me.label2.TabIndex = 44
        Me.label2.Text = "No. Boleto"
        '
        'txtBoleto
        '
        Me.txtBoleto.Location = New System.Drawing.Point(12, 31)
        Me.txtBoleto.Name = "txtBoleto"
        Me.txtBoleto.Size = New System.Drawing.Size(93, 20)
        Me.txtBoleto.TabIndex = 42
        '
        'btnActualiza
        '
        Me.btnActualiza.Location = New System.Drawing.Point(514, 12)
        Me.btnActualiza.Name = "btnActualiza"
        Me.btnActualiza.Size = New System.Drawing.Size(114, 39)
        Me.btnActualiza.TabIndex = 45
        Me.btnActualiza.Text = "Actualiza Boleta"
        Me.btnActualiza.UseVisualStyleBackColor = True
        '
        'button1
        '
        Me.button1.Location = New System.Drawing.Point(634, 12)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(114, 39)
        Me.button1.TabIndex = 46
        Me.button1.Text = "Cerrar"
        Me.button1.UseVisualStyleBackColor = True
        '
        'txtBoletoBack
        '
        Me.txtBoletoBack.Location = New System.Drawing.Point(401, 22)
        Me.txtBoletoBack.Name = "txtBoletoBack"
        Me.txtBoletoBack.Size = New System.Drawing.Size(100, 20)
        Me.txtBoletoBack.TabIndex = 47
        Me.txtBoletoBack.Visible = False
        '
        'btLimpiar
        '
        Me.btLimpiar.BackColor = System.Drawing.Color.Transparent
        Me.btLimpiar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btLimpiar.Location = New System.Drawing.Point(395, 12)
        Me.btLimpiar.Name = "btLimpiar"
        Me.btLimpiar.Size = New System.Drawing.Size(113, 39)
        Me.btLimpiar.TabIndex = 48
        Me.btLimpiar.TabStop = False
        Me.btLimpiar.Text = "Limpiar"
        Me.btLimpiar.UseVisualStyleBackColor = False
        '
        'txtProducto
        '
        Me.txtProducto.Enabled = False
        Me.txtProducto.Location = New System.Drawing.Point(1073, 452)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.ReadOnly = True
        Me.txtProducto.Size = New System.Drawing.Size(190, 20)
        Me.txtProducto.TabIndex = 4
        Me.txtProducto.Visible = False
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Location = New System.Drawing.Point(1067, 410)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.ReadOnly = True
        Me.txtEmpresa.Size = New System.Drawing.Size(196, 20)
        Me.txtEmpresa.TabIndex = 5
        Me.txtEmpresa.Visible = False
        '
        'frmModificacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(922, 399)
        Me.ControlBox = False
        Me.Controls.Add(Me.btLimpiar)
        Me.Controls.Add(Me.button1)
        Me.Controls.Add(Me.btnActualiza)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.txtBoleto)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.txtBoletoBack)
        Me.Controls.Add(Me.txtProducto)
        Me.Controls.Add(Me.txtEmpresa)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmModificacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modificacion de Ticket SIMAPORT"
        Me.panel1.ResumeLayout(False)
        Me.panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents button1 As System.Windows.Forms.Button
    Private WithEvents btnActualiza As System.Windows.Forms.Button
    Private cmbTipoTransporte As System.Windows.Forms.ComboBox
    Private label1 As System.Windows.Forms.Label
    Public WithEvents txtBoleto As System.Windows.Forms.TextBox
    Private label2 As System.Windows.Forms.Label
    Private WithEvents btnConsultar As System.Windows.Forms.Button
    Private txtPlaca As System.Windows.Forms.TextBox
    Private WithEvents txtBruto As System.Windows.Forms.TextBox
    Private label3 As System.Windows.Forms.Label
    Private WithEvents txtTara As System.Windows.Forms.TextBox
    Private txtTagID As System.Windows.Forms.TextBox
    Private label10 As System.Windows.Forms.Label
    Private label5 As System.Windows.Forms.Label
    Private label6 As System.Windows.Forms.Label
    Private label7 As System.Windows.Forms.Label
    Private label8 As System.Windows.Forms.Label
    Private label9 As System.Windows.Forms.Label
    Private WithEvents txtNeto As System.Windows.Forms.TextBox
    Private panel1 As System.Windows.Forms.Panel
    '
    Public custDS As DataSet
    '
    '
    Private Shared conn As New OracleConnection()
    '
    Private Shared connSS As New SqlClient.SqlConnection
    Private Shared connOLE As New OleDb.OleDbConnection
    '
    Private Shared connGrd As New OleDb.OleDbConnection 'SqlClient.SqlConnection
    Private Shared xjmt As New OracleConnection()
    Public vExiste As Boolean
    Friend WithEvents cmbProducto As System.Windows.Forms.ComboBox
    Friend WithEvents cmbEmpresa As System.Windows.Forms.ComboBox
    Private WithEvents txtBoletoBack As System.Windows.Forms.TextBox
    Private WithEvents btLimpiar As System.Windows.Forms.Button
    Private WithEvents txtProducto As System.Windows.Forms.TextBox
    Private WithEvents txtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents cmbTransportista As System.Windows.Forms.ComboBox
    Private WithEvents Label4 As System.Windows.Forms.Label
    Private WithEvents Label11 As System.Windows.Forms.Label
    Private WithEvents txtMarcas As System.Windows.Forms.TextBox
    Private WithEvents Economico As System.Windows.Forms.Label
    Private WithEvents txtEconomico As System.Windows.Forms.TextBox
    Private WithEvents Label12 As System.Windows.Forms.Label
    Private WithEvents txtPedi As System.Windows.Forms.TextBox
    Private WithEvents Label14 As System.Windows.Forms.Label
    Private WithEvents txtBuque As System.Windows.Forms.TextBox
    Private WithEvents Label13 As System.Windows.Forms.Label
    Private WithEvents txtRemesa As System.Windows.Forms.TextBox
End Class
